package dto;

import java.io.Serializable;

public class SecurityQuestionMasterDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7410348180760850410L;
	
	
    private Long questionId;
	
    private String question;
    
    private String answer;
    
    
    


	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "SecurityQuestionMasterDto [question_id=" + questionId + ", question=" + question + "]";
	}

	
	

}
