package dto;

public class RecruiterDTO {

	private String id;

	private String recruiterName;

	private String firstName;   

	private String middleName;   

	private String lastName;   

	private String activeFlag ="Y";

	private String joinDate;

	private String personalEmail;   

	private String officialEmail;

	private String birthDate;

	private String highestQualifincation;

	private String passDate ;

	private String recrutingComments;

	private String salary;

	private String commission;

	private String prevExp;

	private String prevCompWorked;

	private String nativePlace;

	private String addrLine1;

	private String addrLine2;

	private String addrLine3;

	private String town;

	private String district;

	private String state;

	private String zipcode;

	private String permAddrLine1;

	private String permAddrLine2;

	private String permAddrLine3;

	private String permtTown;

	private String permDistrict;

	private String permState;

	private String permZipcode;

	private String userId;
	
	private String corpId;
	
	private String ftinNumber;

	

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}



	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getOfficialEmail() {
		return officialEmail;
	}

	public void setOfficialEmail(String officialEmail) {
		this.officialEmail = officialEmail;
	}


	public String getHighestQualifincation() {
		return highestQualifincation;
	}

	public void setHighestQualifincation(String highestQualifincation) {
		this.highestQualifincation = highestQualifincation;
	}



	public String getRecrutingComments() {
		return recrutingComments;
	}

	public void setRecrutingComments(String recrutingComments) {
		this.recrutingComments = recrutingComments;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getPrevExp() {
		return prevExp;
	}

	public void setPrevExp(String prevExp) {
		this.prevExp = prevExp;
	}

	public String getPrevCompWorked() {
		return prevCompWorked;
	}

	public void setPrevCompWorked(String prevCompWorked) {
		this.prevCompWorked = prevCompWorked;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getAddrLine2() {
		return addrLine2;
	}

	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}

	public String getAddrLine3() {
		return addrLine3;
	}

	public void setAddrLine3(String addrLine3) {
		this.addrLine3 = addrLine3;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassDate() {
		return passDate;
	}

	public void setPassDate(String passDate) {
		this.passDate = passDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPermAddrLine1() {
		return permAddrLine1;
	}

	public void setPermAddrLine1(String permAddrLine1) {
		this.permAddrLine1 = permAddrLine1;
	}

	public String getPermAddrLine2() {
		return permAddrLine2;
	}

	public void setPermAddrLine2(String permAddrLine2) {
		this.permAddrLine2 = permAddrLine2;
	}

	public String getPermAddrLine3() {
		return permAddrLine3;
	}

	public void setPermAddrLine3(String permAddrLine3) {
		this.permAddrLine3 = permAddrLine3;
	}

	public String getPermtTown() {
		return permtTown;
	}

	public void setPermtTown(String permtTown) {
		this.permtTown = permtTown;
	}

	public String getPermDistrict() {
		return permDistrict;
	}

	public void setPermDistrict(String permDistrict) {
		this.permDistrict = permDistrict;
	}

	public String getPermState() {
		return permState;
	}

	public void setPermState(String permState) {
		this.permState = permState;
	}

	public String getPermZipcode() {
		return permZipcode;
	}

	public void setPermZipcode(String permZipcode) {
		this.permZipcode = permZipcode;
	}


	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	@Override
	public String toString() {
		return "Recruiter [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", activeFlag=" + activeFlag + ", joinDate=" + joinDate + ", personalEmail="
				+ personalEmail + ", officialEmail=" + officialEmail + ", birthDate=" + birthDate
				+ ", highestQualifincation=" + highestQualifincation + ", passDate=" + passDate + ", recrutingComments="
				+ recrutingComments + ", salary=" + salary + ", commission=" + commission + ", prevExp=" + prevExp
				+ ", prevCompWorked=" + prevCompWorked + ", nativePlace=" + nativePlace + ", addrLine1=" + addrLine1
				+ ", addrLine2=" + addrLine2 + ", addrLine3=" + addrLine3 + ", town=" + town + ", district=" + district
				+ ", state=" + state + ", zipcode=" + zipcode + ", permAddrLine1=" + permAddrLine1 + ", permAddrLine2="
				+ permAddrLine2 + ", permAddrLine3=" + permAddrLine3 + ", permtTown=" + permtTown + ", permDistrict="
				+ permDistrict + ", permState=" + permState + ", permZipcode=" + permZipcode +  "]";
	}

	public String getFtinNumber() {
		return ftinNumber;
	}

	public void setFtinNumber(String ftinNumber) {
		this.ftinNumber = ftinNumber;
	}
}