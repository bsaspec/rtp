package dto;


import java.util.List;

import dto.SecurityQuestionMasterDto;

public class SecurityQuestionWrapper {
	
	List<SecurityQuestionMasterDto> userCheckins;

	Long userId;
	
	String password;

	public List<SecurityQuestionMasterDto> getUserCheckins() {
		return userCheckins;
	}

	public void setUserCheckins(List<SecurityQuestionMasterDto> userCheckins) {
		this.userCheckins = userCheckins;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	

}
