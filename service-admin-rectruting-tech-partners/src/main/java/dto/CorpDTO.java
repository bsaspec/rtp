package dto;

public class CorpDTO {

	private String id;

	private String userId;

	private String companyName;   

	private String addressLine1;   

	private String addressLine2;  

	private String city;  

	private String state;  

	private String zip; 

	private String eodAlertEmail;   

	private String interviewIntegrationEmail; 

	private String ftinNumber;   

	private String signingAuthorityFirstName;  

	private String signingAuthorityMiddleName;  

	private String signingAuthorityLastName; 

	private String signingAuthorityPhone;   

	private String signingAuthorityDesignation;  

	private String signingAuthorityEmail;  

	private String ownerFirstName;  

	private String ownerMiddleName;  

	private String  ownerLastName; 

	private String ownerPhone; 

	private String ownerAlternatePhone; 

	private String invoiceContactFirstName;  

	private String invoiceContactMiddleName;  

	private String  invoiceContactLastName; 

	private String invoiceContactPhone; 

	private String invoiceContactEmail;  

	private String portalAdminFirstName;  

	private String portalAdminMiddleName;  

	private String  portalAdminLastName; 

	private String portalAdminPhone; 

	private String portalAdminEmail;  

	private String createdByUserId;

	private String updatedByUserId;

	private String enabled;


	public CorpDTO(){

	}

	/**
	 * @return the id
	 */
	 public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	 public void setId(String id) {
		 this.id = id;
	 }

	 /**
	  * @return the companyName
	  */
	 public String getCompanyName() {
		 return companyName;
	 }

	 /**
	  * @param companyName the companyName to set
	  */
	 public void setCompanyName(String companyName) {
		 this.companyName = companyName;
	 }

	 /**
	  * @return the addressLine1
	  */
	 public String getAddressLine1() {
		 return addressLine1;
	 }

	 /**
	  * @param addressLine1 the addressLine1 to set
	  */
	 public void setAddressLine1(String addressLine1) {
		 this.addressLine1 = addressLine1;
	 }

	 /**
	  * @return the addressLine2
	  */
	 public String getAddressLine2() {
		 return addressLine2;
	 }

	 /**
	  * @param addressLine2 the addressLine2 to set
	  */
	 public void setAddressLine2(String addressLine2) {
		 this.addressLine2 = addressLine2;
	 }

	 /**
	  * @return the city
	  */
	 public String getCity() {
		 return city;
	 }

	 /**
	  * @param city the city to set
	  */
	 public void setCity(String city) {
		 this.city = city;
	 }

	 /**
	  * @return the state
	  */
	 public String getState() {
		 return state;
	 }

	 /**
	  * @param state the state to set
	  */
	 public void setState(String state) {
		 this.state = state;
	 }

	 /**
	  * @return the zip
	  */
	 public String getZip() {
		 return zip;
	 }

	 /**
	  * @param zip the zip to set
	  */
	 public void setZip(String zip) {
		 this.zip = zip;
	 }

	 /**
	  * @return the eodAlertEmail
	  */
	 public String getEodAlertEmail() {
		 return eodAlertEmail;
	 }

	 /**
	  * @param eodAlertEmail the eodAlertEmail to set
	  */
	 public void setEodAlertEmail(String eodAlertEmail) {
		 this.eodAlertEmail = eodAlertEmail;
	 }

	 /**
	  * @return the interviewIntegrationEmail
	  */
	 public String getInterviewIntegrationEmail() {
		 return interviewIntegrationEmail;
	 }

	 /**
	  * @param interviewIntegrationEmail the interviewIntegrationEmail to set
	  */
	 public void setInterviewIntegrationEmail(String interviewIntegrationEmail) {
		 this.interviewIntegrationEmail = interviewIntegrationEmail;
	 }

	 /**
	  * @return the ftinNumber
	  */
	 public String getFtinNumber() {
		 return ftinNumber;
	 }

	 /**
	  * @param ftinNumber the ftinNumber to set
	  */
	 public void setFtinNumber(String ftinNumber) {
		 this.ftinNumber = ftinNumber;
	 }

	 /**
	  * @return the signingAuthorityFirstName
	  */
	 public String getSigningAuthorityFirstName() {
		 return signingAuthorityFirstName;
	 }

	 /**
	  * @param signingAuthorityFirstName the signingAuthorityFirstName to set
	  */
	 public void setSigningAuthorityFirstName(String signingAuthorityFirstName) {
		 this.signingAuthorityFirstName = signingAuthorityFirstName;
	 }

	 /**
	  * @return the signingAuthorityMiddleName
	  */
	 public String getSigningAuthorityMiddleName() {
		 return signingAuthorityMiddleName;
	 }

	 /**
	  * @param signingAuthorityMiddleName the signingAuthorityMiddleName to set
	  */
	 public void setSigningAuthorityMiddleName(String signingAuthorityMiddleName) {
		 this.signingAuthorityMiddleName = signingAuthorityMiddleName;
	 }

	 /**
	  * @return the signingAuthorityLastName
	  */
	 public String getSigningAuthorityLastName() {
		 return signingAuthorityLastName;
	 }

	 /**
	  * @param signingAuthorityLastName the signingAuthorityLastName to set
	  */
	 public void setSigningAuthorityLastName(String signingAuthorityLastName) {
		 this.signingAuthorityLastName = signingAuthorityLastName;
	 }

	 /**
	  * @return the signingAuthorityPhone
	  */
	 public String getSigningAuthorityPhone() {
		 return signingAuthorityPhone;
	 }

	 /**
	  * @param signingAuthorityPhone the signingAuthorityPhone to set
	  */
	 public void setSigningAuthorityPhone(String signingAuthorityPhone) {
		 this.signingAuthorityPhone = signingAuthorityPhone;
	 }

	 /**
	  * @return the signingAuthorityDesignation
	  */
	 public String getSigningAuthorityDesignation() {
		 return signingAuthorityDesignation;
	 }

	 /**
	  * @param signingAuthorityDesignation the signingAuthorityDesignation to set
	  */
	 public void setSigningAuthorityDesignation(String signingAuthorityDesignation) {
		 this.signingAuthorityDesignation = signingAuthorityDesignation;
	 }

	 /**
	  * @return the signingAuthorityEmail
	  */
	 public String getSigningAuthorityEmail() {
		 return signingAuthorityEmail;
	 }

	 /**
	  * @param signingAuthorityEmail the signingAuthorityEmail to set
	  */
	 public void setSigningAuthorityEmail(String signingAuthorityEmail) {
		 this.signingAuthorityEmail = signingAuthorityEmail;
	 }

	 /**
	  * @return the ownerFirstName
	  */
	 public String getOwnerFirstName() {
		 return ownerFirstName;
	 }

	 /**
	  * @param ownerFirstName the ownerFirstName to set
	  */
	 public void setOwnerFirstName(String ownerFirstName) {
		 this.ownerFirstName = ownerFirstName;
	 }

	 /**
	  * @return the ownerMiddleName
	  */
	 public String getOwnerMiddleName() {
		 return ownerMiddleName;
	 }

	 /**
	  * @param ownerMiddleName the ownerMiddleName to set
	  */
	 public void setOwnerMiddleName(String ownerMiddleName) {
		 this.ownerMiddleName = ownerMiddleName;
	 }

	 /**
	  * @return the ownerLastName
	  */
	 public String getOwnerLastName() {
		 return ownerLastName;
	 }

	 /**
	  * @param ownerLastName the ownerLastName to set
	  */
	 public void setOwnerLastName(String ownerLastName) {
		 this.ownerLastName = ownerLastName;
	 }

	 /**
	  * @return the ownerPhone
	  */
	 public String getOwnerPhone() {
		 return ownerPhone;
	 }

	 /**
	  * @param ownerPhone the ownerPhone to set
	  */
	 public void setOwnerPhone(String ownerPhone) {
		 this.ownerPhone = ownerPhone;
	 }

	 /**
	  * @return the ownerAlternatePhone
	  */
	 public String getOwnerAlternatePhone() {
		 return ownerAlternatePhone;
	 }

	 /**
	  * @param ownerAlternatePhone the ownerAlternatePhone to set
	  */
	 public void setOwnerAlternatePhone(String ownerAlternatePhone) {
		 this.ownerAlternatePhone = ownerAlternatePhone;
	 }

	 /**
	  * @return the invoiceContactFirstName
	  */
	 public String getInvoiceContactFirstName() {
		 return invoiceContactFirstName;
	 }

	 /**
	  * @param invoiceContactFirstName the invoiceContactFirstName to set
	  */
	 public void setInvoiceContactFirstName(String invoiceContactFirstName) {
		 this.invoiceContactFirstName = invoiceContactFirstName;
	 }

	 /**
	  * @return the invoiceContactMiddleName
	  */
	 public String getInvoiceContactMiddleName() {
		 return invoiceContactMiddleName;
	 }

	 /**
	  * @param invoiceContactMiddleName the invoiceContactMiddleName to set
	  */
	 public void setInvoiceContactMiddleName(String invoiceContactMiddleName) {
		 this.invoiceContactMiddleName = invoiceContactMiddleName;
	 }

	 /**
	  * @return the invoiceContactLastName
	  */
	 public String getInvoiceContactLastName() {
		 return invoiceContactLastName;
	 }

	 /**
	  * @param invoiceContactLastName the invoiceContactLastName to set
	  */
	 public void setInvoiceContactLastName(String invoiceContactLastName) {
		 this.invoiceContactLastName = invoiceContactLastName;
	 }

	 /**
	  * @return the invoiceContactPhone
	  */
	 public String getInvoiceContactPhone() {
		 return invoiceContactPhone;
	 }

	 /**
	  * @param invoiceContactPhone the invoiceContactPhone to set
	  */
	 public void setInvoiceContactPhone(String invoiceContactPhone) {
		 this.invoiceContactPhone = invoiceContactPhone;
	 }

	 /**
	  * @return the invoiceContactEmail
	  */
	 public String getInvoiceContactEmail() {
		 return invoiceContactEmail;
	 }

	 /**
	  * @param invoiceContactEmail the invoiceContactEmail to set
	  */
	 public void setInvoiceContactEmail(String invoiceContactEmail) {
		 this.invoiceContactEmail = invoiceContactEmail;
	 }

	 /**
	  * @return the portalAdminFirstName
	  */
	 public String getPortalAdminFirstName() {
		 return portalAdminFirstName;
	 }

	 /**
	  * @param portalAdminFirstName the portalAdminFirstName to set
	  */
	 public void setPortalAdminFirstName(String portalAdminFirstName) {
		 this.portalAdminFirstName = portalAdminFirstName;
	 }

	 /**
	  * @return the portalAdminMiddleName
	  */
	 public String getPortalAdminMiddleName() {
		 return portalAdminMiddleName;
	 }

	 /**
	  * @param portalAdminMiddleName the portalAdminMiddleName to set
	  */
	 public void setPortalAdminMiddleName(String portalAdminMiddleName) {
		 this.portalAdminMiddleName = portalAdminMiddleName;
	 }

	 /**
	  * @return the portalAdminLastName
	  */
	 public String getPortalAdminLastName() {
		 return portalAdminLastName;
	 }

	 /**
	  * @param portalAdminLastName the portalAdminLastName to set
	  */
	 public void setPortalAdminLastName(String portalAdminLastName) {
		 this.portalAdminLastName = portalAdminLastName;
	 }

	 /**
	  * @return the portalAdminPhone
	  */
	 public String getPortalAdminPhone() {
		 return portalAdminPhone;
	 }

	 /**
	  * @param portalAdminPhone the portalAdminPhone to set
	  */
	 public void setPortalAdminPhone(String portalAdminPhone) {
		 this.portalAdminPhone = portalAdminPhone;
	 }

	 /**
	  * @return the portalAdminEmail
	  */
	 public String getPortalAdminEmail() {
		 return portalAdminEmail;
	 }

	 /**
	  * @param portalAdminEmail the portalAdminEmail to set
	  */
	 public void setPortalAdminEmail(String portalAdminEmail) {
		 this.portalAdminEmail = portalAdminEmail;
	 }


	 /**
	  * @return the enabled
	  */
	 public String getEnabled() {
		 return enabled;
	 }

	 /**
	  * @param enabled the enabled to set
	  */
	 public void setEnabled(String enabled) {
		 this.enabled = enabled;
	 }

	

	 /**
	  * @return the createdByUserId
	  */
	 public String getCreatedByUserId() {
		 return createdByUserId;
	 }

	 /**
	  * @param createdByUserId the createdByUserId to set
	  */
	 public void setCreatedByUserId(String createdByUserId) {
		 this.createdByUserId = createdByUserId;
	 }

	 /**
	  * @return the updatedByUserId
	  */
	 public String getUpdatedByUserId() {
		 return updatedByUserId;
	 }

	 /**
	  * @param updatedByUserId the updatedByUserId to set
	  */
	 public void setUpdatedByUserId(String updatedByUserId) {
		 this.updatedByUserId = updatedByUserId;
	 }

	 /**
	  * @return the userId
	  */
	 public String getUserId() {
		 return userId;
	 }

	 /**
	  * @param userId the userId to set
	  */
	 public void setUserId(String userId) {
		 this.userId = userId;
	 }
}