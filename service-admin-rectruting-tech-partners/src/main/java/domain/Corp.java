package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "corp")
public class Corp implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue 
    @Column(name="id")
    private Long id;

	@Column(name = "name")
    private String companyName;   
	
	@Column(name = "addr_line_1")
    private String addressLine1;   
	
	@Column(name = "addr_line_2")
    private String addressLine2;  
	
	@Column(name = "city")
    private String city;  
	
	@Column(name = "state")
    private String state;  
		
	@Column(name = "zip")
    private String zip; 
	
	@Column(name = "eod_alert_email")
    private String eodAlertEmail;   
	
	@Column(name = "intrvw_integration_email")
    private String interviewIntegrationEmail; 

	@Column(name = "ftin_number")
    private Long ftinNumber;   

	@Column(name = "signing_auth_fname")
    private String signingAuthorityFirstName;  
	
	@Column(name = "signing_auth_mname")
    private String signingAuthorityMiddleName;  
	
	@Column(name = "signing_auth_lname")
    private String signingAuthorityLastName; 
	
	@Column(name = "signing_auth_phone")
    private String signingAuthorityPhone;   
	
	@Column(name = "signing_auth_desig")
    private String signingAuthorityDesignation;  
	
	@Column(name = "signing_auth_email")
    private String signingAuthorityEmail;  
	
	@Column(name = "owner_fname")
    private String ownerFirstName;  
	
	@Column(name = "owner_mname")
    private String ownerMiddleName;  
	
	@Column(name = "owner_lname")
    private String  ownerLastName; 
	
	@Column(name = "owner_phone")
    private String ownerPhone; 
	
	@Column(name = "owner_alt_phone")
    private String ownerAlternatePhone; 
	
	@Column(name = "invoice_contact_fname")
    private String invoiceContactFirstName;  
	
	@Column(name = "invoice_contact_mname")
    private String invoiceContactMiddleName;  
	
	@Column(name = "invoice_contact_lname")
    private String  invoiceContactLastName; 
	
	@Column(name = "invoice_contact_phone")
    private String invoiceContactPhone; 
	
	@Column(name = "invoice_contact_email")
    private String invoiceContactEmail;  
	
	@Column(name = "portal_admin_fname")
    private String portalAdminFirstName;  
	
	@Column(name = "portal_admin_mname")
    private String portalAdminMiddleName;  
	
	@Column(name = "portal_admin_lname")
    private String  portalAdminLastName; 
	
	@Column(name = "portal_admin_phone")
    private String portalAdminPhone; 
	
	@Column(name = "portal_admin_email")
    private String portalAdminEmail;  
	
	@Column(name ="enable_flg")
	private String enabled ="Y";

	@Column(name ="create_dttm")
	private Date createdDtTm;
	
	@Column(name ="update_dttm")
	private Date updatdDtTm;
	
	@Column(name ="create_user")
	private String createdByUserId;
	
	@Column(name ="update_user")
	private String updatedByUserId;
	
	@ManyToMany(mappedBy="corpRoles",cascade = CascadeType.PERSIST)
	private Set<User> users = new HashSet<User>();
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public Corp(){
		
	}

	public Long getId() {
		return id;
	}

	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}



	public String getAddressLine1() {
		return addressLine1;
	}



	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}



	public String getAddressLine2() {
		return addressLine2;
	}



	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getZip() {
		return zip;
	}



	public void setZip(String zip) {
		this.zip = zip;
	}



	public String getEodAlertEmail() {
		return eodAlertEmail;
	}



	public void setEodAlertEmail(String eodAlertEmail) {
		this.eodAlertEmail = eodAlertEmail;
	}



	public String getInterviewIntegrationEmail() {
		return interviewIntegrationEmail;
	}



	public void setInterviewIntegrationEmail(String interviewIntegrationEmail) {
		this.interviewIntegrationEmail = interviewIntegrationEmail;
	}







	public String getSigningAuthorityFirstName() {
		return signingAuthorityFirstName;
	}



	public void setSigningAuthorityFirstName(String signingAuthorityFirstName) {
		this.signingAuthorityFirstName = signingAuthorityFirstName;
	}



	public String getSigningAuthorityMiddleName() {
		return signingAuthorityMiddleName;
	}



	public void setSigningAuthorityMiddleName(String signingAuthorityMiddleName) {
		this.signingAuthorityMiddleName = signingAuthorityMiddleName;
	}



	public String getSigningAuthorityLastName() {
		return signingAuthorityLastName;
	}



	public void setSigningAuthorityLastName(String signingAuthorityLastName) {
		this.signingAuthorityLastName = signingAuthorityLastName;
	}



	public String getSigningAuthorityPhone() {
		return signingAuthorityPhone;
	}



	public void setSigningAuthorityPhone(String signingAuthorityPhone) {
		this.signingAuthorityPhone = signingAuthorityPhone;
	}



	public String getSigningAuthorityDesignation() {
		return signingAuthorityDesignation;
	}



	public void setSigningAuthorityDesignation(String signingAuthorityDesignation) {
		this.signingAuthorityDesignation = signingAuthorityDesignation;
	}



	public String getSigningAuthorityEmail() {
		return signingAuthorityEmail;
	}



	public void setSigningAuthorityEmail(String signingAuthorityEmail) {
		this.signingAuthorityEmail = signingAuthorityEmail;
	}



	public String getOwnerFirstName() {
		return ownerFirstName;
	}



	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}



	public String getOwnerMiddleName() {
		return ownerMiddleName;
	}



	public void setOwnerMiddleName(String ownerMiddleName) {
		this.ownerMiddleName = ownerMiddleName;
	}



	public String getOwnerLastName() {
		return ownerLastName;
	}



	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}



	public String getOwnerPhone() {
		return ownerPhone;
	}



	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}



	public String getOwnerAlternatePhone() {
		return ownerAlternatePhone;
	}



	public void setOwnerAlternatePhone(String ownerAlternatePhone) {
		this.ownerAlternatePhone = ownerAlternatePhone;
	}



	public String getInvoiceContactFirstName() {
		return invoiceContactFirstName;
	}



	public void setInvoiceContactFirstName(String invoiceContactFirstName) {
		this.invoiceContactFirstName = invoiceContactFirstName;
	}



	public String getInvoiceContactMiddleName() {
		return invoiceContactMiddleName;
	}



	public void setInvoiceContactMiddleName(String invoiceContactMiddleName) {
		this.invoiceContactMiddleName = invoiceContactMiddleName;
	}



	public String getInvoiceContactLastName() {
		return invoiceContactLastName;
	}



	public void setInvoiceContactLastName(String invoiceContactLastName) {
		this.invoiceContactLastName = invoiceContactLastName;
	}



	public String getInvoiceContactPhone() {
		return invoiceContactPhone;
	}

	public void setInvoiceContactPhone(String invoiceContactPhone) {
		this.invoiceContactPhone = invoiceContactPhone;
	}


	public Long getFtinNumber() {
		return ftinNumber;
	}



	public void setFtinNumber(Long ftinNumber) {
		this.ftinNumber = ftinNumber;
	}




	public String getInvoiceContactEmail() {
		return invoiceContactEmail;
	}



	public void setInvoiceContactEmail(String invoiceContactEmail) {
		this.invoiceContactEmail = invoiceContactEmail;
	}



	public String getPortalAdminFirstName() {
		return portalAdminFirstName;
	}



	public void setPortalAdminFirstName(String portalAdminFirstName) {
		this.portalAdminFirstName = portalAdminFirstName;
	}



	public String getPortalAdminMiddleName() {
		return portalAdminMiddleName;
	}



	public void setPortalAdminMiddleName(String portalAdminMiddleName) {
		this.portalAdminMiddleName = portalAdminMiddleName;
	}



	public String getPortalAdminLastName() {
		return portalAdminLastName;
	}



	public void setPortalAdminLastName(String portalAdminLastName) {
		this.portalAdminLastName = portalAdminLastName;
	}



	public String getPortalAdminPhone() {
		return portalAdminPhone;
	}



	public void setPortalAdminPhone(String portalAdminPhone) {
		this.portalAdminPhone = portalAdminPhone;
	}



	public String getPortalAdminEmail() {
		return portalAdminEmail;
	}



	public void setPortalAdminEmail(String portalAdminEmail) {
		this.portalAdminEmail = portalAdminEmail;
	}



	public void setId(Long id) {
		this.id = id;
	}

	
	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	public Date getUpdatdDtTm() {
		return updatdDtTm;
	}

	public void setUpdatdDtTm(Date updatdDtTm) {
		this.updatdDtTm = updatdDtTm;
	}

	public String getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	public String getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(String updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}



	@Override
	public String toString() {
		return "Corp [id=" + id + ", companyName=" + companyName + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", city=" + city + ", state=" + state + ", zip=" + zip + ", eodAlertEmail="
				+ eodAlertEmail + ", interviewIntegrationEmail=" + interviewIntegrationEmail + ", ftinNumber="
				+ ftinNumber + ", signingAuthorityFirstName=" + signingAuthorityFirstName
				+ ", signingAuthorityMiddleName=" + signingAuthorityMiddleName + ", signingAuthorityLastName="
				+ signingAuthorityLastName + ", signingAuthorityPhone=" + signingAuthorityPhone
				+ ", signingAuthorityDesignation=" + signingAuthorityDesignation + ", signingAuthorityEmail="
				+ signingAuthorityEmail + ", ownerFirstName=" + ownerFirstName + ", ownerMiddleName=" + ownerMiddleName
				+ ", ownerLastName=" + ownerLastName + ", ownerPhone=" + ownerPhone + ", ownerAlternatePhone="
				+ ownerAlternatePhone + ", invoiceContactFirstName=" + invoiceContactFirstName
				+ ", invoiceContactMiddleName=" + invoiceContactMiddleName + ", invoiceContactLastName="
				+ invoiceContactLastName + ", invoiceContactPhone=" + invoiceContactPhone + ", invoiceContactEmail="
				+ invoiceContactEmail + ", portalAdminFirstName=" + portalAdminFirstName + ", portalAdminMiddleName="
				+ portalAdminMiddleName + ", portalAdminLastName=" + portalAdminLastName + ", portalAdminPhone="
				+ portalAdminPhone + ", portalAdminEmail=" + portalAdminEmail + ", enabled=" + enabled
				+ ", createdDtTm=" + createdDtTm + ", updatdDtTm=" + updatdDtTm + ", createdByUserId="
				+ createdByUserId + ", updatedByUserId=" + updatedByUserId + ", users=" + users + "]";
	}

 
}