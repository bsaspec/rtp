package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "USR")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue  
	@Column(name="id")
	private Long id;

	@Column(name = "user_name")
	private String userName;   

	@Column(name = "password")
	private String password;   

	@Column(name = "email")
	private String email;

	@Column(name ="enable_flg")
	private String enabled ="Y";

	@Column(name = "last_name")
	private String lastName;   

	@Column(name = "first_name")
	private String firstName; 

	@Column(name ="create_dttm")
	private  Date createdDtTm;

	@Column(name ="update_dttm")
	private Date updatdDtTm;

	@Column(name ="create_user")
	private String createdByUserId;

	@Column(name ="update_user")
	private String updatedByUserId;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	@Fetch(FetchMode.SELECT)
	private Set<UserSecurityQuestion> userSecurityQuestion=new HashSet<UserSecurityQuestion>(0);
	
	@ManyToMany(cascade = {CascadeType.ALL},fetch=FetchType.EAGER)
	@JoinTable(name="usr_corp_role", 
				joinColumns={@JoinColumn(name="user_id")}, 
				inverseJoinColumns={@JoinColumn(name="role_id")})
	@MapKeyJoinColumn(name = "corp_id")
    @ElementCollection
    private Map<Corp, UserRole> corpRoles = new HashMap<Corp, UserRole>();

	public Map<Corp, UserRole> getCorpRoles() {
		return corpRoles;
	}

	public void setCorpRoles(Map<Corp, UserRole> corpRoles) {
		this.corpRoles = corpRoles;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public User(){

	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", email=" + email
				+ ", enabled=" + enabled + ", lastName=" + lastName + ", firstName=" + firstName 
				+ ", createdDtTm=" + createdDtTm + ", updatdDtTm=" + updatdDtTm 
				+ ", createdByUserId=" + createdByUserId + ", updatedByUserId=" + updatedByUserId + "]";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the enabled
	 */
	public String getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	/**
	 * @return the updatdDtTm
	 */
	public Date getUpdatdDtTm() {
		return updatdDtTm;
	}

	/**
	 * @param updatdDtTm the updatdDtTm to set
	 */
	public void setUpdatdDtTm(Date updatdDtTm) {
		this.updatdDtTm = updatdDtTm;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the updatedByUserId
	 */
	public String getUpdatedByUserId() {
		return updatedByUserId;
	}

	/**
	 * @param updatedByUserId the updatedByUserId to set
	 */
	public void setUpdatedByUserId(String updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	public Set<UserSecurityQuestion> getUserSecurityQuestion() {
		return userSecurityQuestion;
	}

	public void setUserSecurityQuestion(Set<UserSecurityQuestion> userSecurityQuestion) {
		this.userSecurityQuestion = userSecurityQuestion;
	}

	
}