package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "recruiter")
public class Recruiter implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    @Column(name="id")
    private Long id;
	
	@Column(name="user_id")
    private Long userId;

	@Column(name = "name")
    private String recruiterName;
	
	@Column(name = "first_name")
    private String firstName;   
	
	@Column(name = "middle_name")
    private String middleName;   
	
	@Column(name = "last_name")
    private String lastName;   

	@Column(name = "enable_flg")
    private String activeFlag ="Y";
    
	@Column(name ="join_date")
	private Date joinDate;
	
	@Column(name = "personal_email")
    private String personalEmail;   

	@Column(name = "official_email")
    private String officialEmail;
	
	@Column(name ="birth_date")
	private Date birthDate;
	
	@Column(name = "highest_qualification")
    private String highestQualifincation;
	
	@Column(name ="pass_date")
	private Date passDate ;
	
	@Column(name = "recruting_comments")
    private String recrutingComments;
	
	@Column(name = "salary")
    private String salary;
	
	@Column(name = "commission")
    private String commission;
	
	@Column(name = "prev_exp")
    private String prevExp;
	
	@Column(name = "prev_comp_worked")
    private String prevCompWorked;
	
	@Column(name = "native_place")
    private String nativePlace;
	
	@Column(name = "addr_line_1")
    private String addrLine1;
	
	@Column(name = "addr_line_2")
    private String addrLine2;
	
	@Column(name = "addr_line_3")
    private String addrLine3;
	
	@Column(name = "town")
    private String town;
	
	@Column(name = "district")
    private String district;
	
	@Column(name = "state")
    private String state;
	
	@Column(name = "zipcode")
    private String zipcode;
	
	@Column(name = "perm_addr_line1")
    private String permAddrLine1;
	
	@Column(name = "perm_addr_line2")
    private String permAddrLine2;
	
	@Column(name = "permAddrLine3")
    private String permAddrLine3;
	
	@Column(name = "perm_town")
    private String permtTown;
	
	@Column(name = "perm_district")
    private String permDistrict;
	
	@Column(name = "perm_state")
    private String permState;
	
	@Column(name = "perm_zipcode")
    private String permZipcode;
	
	@Column(name ="create_dttm")
	private Date createdDtTm;
	
	@Column(name ="update_dttm")
	private Date updatdDtTm;
	
	@Column(name ="create_user")
	private String createdByUserId;
	
	@Column(name ="update_user")
	private String updatedByUserId;
	
	
	public Recruiter(){
		
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}



	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getOfficialEmail() {
		return officialEmail;
	}

	public void setOfficialEmail(String officialEmail) {
		this.officialEmail = officialEmail;
	}


	public String getHighestQualifincation() {
		return highestQualifincation;
	}

	public void setHighestQualifincation(String highestQualifincation) {
		this.highestQualifincation = highestQualifincation;
	}



	public String getRecrutingComments() {
		return recrutingComments;
	}

	public void setRecrutingComments(String recrutingComments) {
		this.recrutingComments = recrutingComments;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getPrevExp() {
		return prevExp;
	}

	public void setPrevExp(String prevExp) {
		this.prevExp = prevExp;
	}

	public String getPrevCompWorked() {
		return prevCompWorked;
	}

	public void setPrevCompWorked(String prevCompWorked) {
		this.prevCompWorked = prevCompWorked;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getAddrLine2() {
		return addrLine2;
	}

	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}

	public String getAddrLine3() {
		return addrLine3;
	}

	public void setAddrLine3(String addrLine3) {
		this.addrLine3 = addrLine3;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getPassDate() {
		return passDate;
	}

	public void setPassDate(Date passDate) {
		this.passDate = passDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPermAddrLine1() {
		return permAddrLine1;
	}

	public void setPermAddrLine1(String permAddrLine1) {
		this.permAddrLine1 = permAddrLine1;
	}

	public String getPermAddrLine2() {
		return permAddrLine2;
	}

	public void setPermAddrLine2(String permAddrLine2) {
		this.permAddrLine2 = permAddrLine2;
	}

	public String getPermAddrLine3() {
		return permAddrLine3;
	}

	public void setPermAddrLine3(String permAddrLine3) {
		this.permAddrLine3 = permAddrLine3;
	}

	public String getPermtTown() {
		return permtTown;
	}

	public void setPermtTown(String permtTown) {
		this.permtTown = permtTown;
	}

	public String getPermDistrict() {
		return permDistrict;
	}

	public void setPermDistrict(String permDistrict) {
		this.permDistrict = permDistrict;
	}

	public String getPermState() {
		return permState;
	}

	public void setPermState(String permState) {
		this.permState = permState;
	}

	public String getPermZipcode() {
		return permZipcode;
	}

	public void setPermZipcode(String permZipcode) {
		this.permZipcode = permZipcode;
	}

	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	public Date getUpdatdDtTm() {
		return updatdDtTm;
	}

	public void setUpdatdDtTm(Date updatdDtTm) {
		this.updatdDtTm = updatdDtTm;
	}

	public String getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	public String getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(String updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}
	
	
	
	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	@Override
	public String toString() {
		return "Recruiter [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", activeFlag=" + activeFlag + ", joinDate=" + joinDate + ", personalEmail="
				+ personalEmail + ", officialEmail=" + officialEmail + ", birthDate=" + birthDate
				+ ", highestQualifincation=" + highestQualifincation + ", passDate=" + passDate + ", recrutingComments="
				+ recrutingComments + ", salary=" + salary + ", commission=" + commission + ", prevExp=" + prevExp
				+ ", prevCompWorked=" + prevCompWorked + ", nativePlace=" + nativePlace + ", addrLine1=" + addrLine1
				+ ", addrLine2=" + addrLine2 + ", addrLine3=" + addrLine3 + ", town=" + town + ", district=" + district
				+ ", state=" + state + ", zipcode=" + zipcode + ", permAddrLine1=" + permAddrLine1 + ", permAddrLine2="
				+ permAddrLine2 + ", permAddrLine3=" + permAddrLine3 + ", permtTown=" + permtTown + ", permDistrict="
				+ permDistrict + ", permState=" + permState + ", permZipcode=" + permZipcode + ", createdDtTm="
				+ createdDtTm + ", updatdDtTm=" + updatdDtTm + ", createdByUserId=" + createdByUserId
				+ ", updatedByUserId=" + updatedByUserId + "]";
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
}