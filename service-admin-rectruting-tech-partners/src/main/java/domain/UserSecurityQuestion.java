package domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usr_security_question")
public class UserSecurityQuestion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 526907138430142979L;
	
	
	@Id @GeneratedValue 
    @Column(name="user_security_questionid")
    private Long user_security_questionid;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	private User user;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="question_id")
	private SecurityQuestionMaster securityQuestionMaster;
	
	@Column(name="answer")
	private String answer;


	public Long getUser_security_questionid() {
		return user_security_questionid;
	}

	public void setUser_security_questionid(Long user_security_questionid) {
		this.user_security_questionid = user_security_questionid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public SecurityQuestionMaster getSecurityQuestionMaster() {
		return securityQuestionMaster;
	}

	public void setSecurityQuestionMaster(SecurityQuestionMaster securityQuestionMaster) {
		this.securityQuestionMaster = securityQuestionMaster;
	}


	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}



}
