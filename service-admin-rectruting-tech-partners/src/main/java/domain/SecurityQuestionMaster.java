package domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "security_question_master")
public class SecurityQuestionMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1604413587910299625L;
	
	
	@Id @GeneratedValue 
    @Column(name="question_id")
    private Long questionId;
	
	@Column(name = "question")
    private String question;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "securityQuestionMaster")
	@Fetch(FetchMode.SELECT)
	private Set<UserSecurityQuestion> securityQuestions=new  HashSet<UserSecurityQuestion>();


	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Set<UserSecurityQuestion> getSecurityQuestions() {
		return securityQuestions;
	}

	public void setSecurityQuestions(Set<UserSecurityQuestion> securityQuestions) {
		this.securityQuestions = securityQuestions;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	
	

}
