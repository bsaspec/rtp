package service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import config.ConvertUtil;
import dao.RecruiterDao;
import domain.Corp;
import domain.Recruiter;
import domain.User;
import domain.UserRole;
import dto.RecruiterDTO;
import repository.CorpRepository;
import repository.RecruiterRepository;
import repository.UserRepository;


@Service("recruiterService")
@Transactional
public class RecruiterServiceImpl implements RecruiterService{
	
	@Autowired
	RecruiterRepository recruiterRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	CorpRepository corpRepository;
	
	@Autowired
	RecruiterDao recruiterDao;
	
	
	@Transactional
	public Recruiter findRecruiterDistinctByRecruiterName(String recruiterName){
		return recruiterRepository.findRecruiterDistinctByRecruiterName(recruiterName);
	}
	
	@Transactional
	public Recruiter findRecruiterDistinctById(Long id){
		return recruiterRepository.findRecruiterDistinctById(id);
	}
	
	@Transactional
	public RecruiterDTO saveRecruiter(RecruiterDTO recruiterDTO) throws Exception{
		
		Corp finalCorp = corpRepository.findCorpDistinctById(Long.parseLong(recruiterDTO.getCorpId()));
		User user = ConvertUtil.getUserObjectForRecruiter(recruiterDTO);
		UserRole userRole = ConvertUtil.getUserRoleObjectForRecruiter();
		finalCorp.setUsers(null);

		Map<Corp, UserRole> map=new HashMap<Corp, UserRole>();
		map.put(finalCorp, userRole);

		user.setCorpRoles(map);
		System.out.println(user);
		User dbUser = userRepository.save(user);
		
		Recruiter finalRecruiter = ConvertUtil.convertRecruiterDTOToRecruiterObj(recruiterDTO);
		
		Timestamp timestamp = new Timestamp(new Date().getTime());
		finalRecruiter.setCreatedDtTm(timestamp);
		finalRecruiter.setUpdatdDtTm(timestamp);
		finalRecruiter.setUserId(dbUser.getId());
		System.out.println("recruiterDTO :" + recruiterDTO);
		
		Recruiter recruiter =  recruiterRepository.save(finalRecruiter);
		
		RecruiterDTO newRecruiterDTO =  ConvertUtil.convertRecruiterObjToRecruiterDTO(recruiter);
		newRecruiterDTO.setCorpId(recruiterDTO.getCorpId());
		newRecruiterDTO.setUserId(recruiterDTO.getUserId());
		return newRecruiterDTO;
	}
	
	public List<RecruiterDTO> findAllRecruiters() throws Exception { 
		List<RecruiterDTO> recruiterDTOsList = null;
		List<Recruiter> recruiterList =  recruiterRepository.getRecruiters();
		if(recruiterList != null && recruiterList.size() > 0){
			recruiterDTOsList = ConvertUtil.convertRecruiterListToRecruiterDTOList(recruiterList);
		}
		return recruiterDTOsList;
	}
	
	public List<RecruiterDTO> searchRecruiterDetails(String recruiterName,String ftinNumber) throws Exception {
		Corp corp = null;
		if(ftinNumber != null && !"".equalsIgnoreCase(ftinNumber)){
			corp  = corpRepository.findCorpDistinctByFtinNumber(Long.parseLong(ftinNumber));
		}
		return  recruiterDao.searchRecruiterDetails(recruiterName,corp);
	}
	
	public RecruiterDTO deleteRecruiter(Long id) throws Exception {
		Recruiter recruiter = recruiterRepository.findRecruiterDistinctById(id);
		recruiter.setActiveFlag("N");
		Timestamp timestamp = new Timestamp(new Date().getTime());
		recruiter.setUpdatdDtTm(timestamp);
		Recruiter dbRecruiter =  recruiterRepository.save(recruiter);
		return  ConvertUtil.convertRecruiterObjToRecruiterDTO(dbRecruiter);
	}
	

	public RecruiterDTO updateRecruiter(RecruiterDTO recruiterDTO) throws Exception {
		if(recruiterDTO.getId() != null && !"".equalsIgnoreCase(recruiterDTO.getId())){
			Recruiter recruiter = recruiterRepository.findRecruiterDistinctById(Long.parseLong(recruiterDTO.getId()));
			recruiter =  ConvertUtil.convertRecruiterDTOToRecruiterObj(recruiterDTO,recruiter);
			return ConvertUtil.convertRecruiterObjToRecruiterDTO(recruiterRepository.save(recruiter));
		}
		return null;
	}
	
	
	public List<RecruiterDTO> findAllActiveRecruiters() throws Exception { 
		List<RecruiterDTO> recruiterDTOsList = null;
		List<Recruiter> recruiterList =  recruiterRepository.getRecruiters();
		if(recruiterList != null && recruiterList.size() > 0){
			recruiterDTOsList = ConvertUtil.convertActiveRecruiterListToActiveRecruiterDTOList(recruiterList);
		}
		return recruiterDTOsList;
	}
	
	public RecruiterDTO getRecruiter(Long id) {
		return ConvertUtil.convertRecruiterObjToRecruiterDTO(recruiterRepository.findRecruiterDistinctById(id));
	}
}


