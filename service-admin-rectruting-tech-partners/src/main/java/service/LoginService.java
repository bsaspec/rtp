package service;

import java.util.List;

import dto.SecurityQuestionMasterDto;
import dto.SecurityQuestionWrapper;
import dto.UserDTO;
import dto.UserSecurityQuestionDto;

public interface LoginService {
	
	UserDTO loginUser(String userName,String password) throws Exception;

	List<SecurityQuestionMasterDto> getSecurityQuestions();

	List<UserSecurityQuestionDto> getCheckUser(Long questionId);


	Object saveAnswers(SecurityQuestionWrapper securityQuestionWrapper);

}
