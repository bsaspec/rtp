package service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import config.ConvertUtil;
import dao.UserDao;
import domain.Corp;
import domain.User;
import domain.UserRole;
import dto.UserDTO;
import repository.CorpRepository;
import repository.UserRepository;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	
	
	
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	CorpRepository corpRepository;
	
	@Autowired
	private UserDao userDAO;
	
	public UserDTO saveUser(UserDTO userDTO) throws Exception{
		Corp finalCorp = corpRepository.findCorpDistinctById(Long.parseLong(userDTO.getCorpId()));
		User dbUser = userRepository.findUserDistinctById(Long.parseLong(userDTO.getUserId()));
		User user = ConvertUtil.convertUserDTOToUserObj(userDTO,dbUser);
		UserRole userRole = ConvertUtil.getUserRoleObject();
		finalCorp.setUsers(null);

		Map<Corp, UserRole> map=new HashMap<Corp, UserRole>();
		map.put(finalCorp, userRole);

		user.setCorpRoles(map);
		System.out.println(user);
		User newUser = userRepository.save(user);
		
		return ConvertUtil.convertUserObjToUserDTO(newUser);
	}
	public User findUserDistinctByUserName(String userName)throws Exception{
		return userRepository.findUserDistinctByUserName(userName);
		
	}
	
	public List<UserDTO> findAllUsers() throws Exception { 
		List<UserDTO> userDTOsList = null;
		List<User> userList =  userRepository.getAllActiveAdminUsers();
		if(userList != null && userList.size() > 0){
			userDTOsList = ConvertUtil.convertUserListToUserDTOList(userList);
		}
		return userDTOsList;
	}
	
	public List<UserDTO> searchUserDetails(UserDTO userDTO) throws Exception { 
		List<UserDTO> userList =  userDAO.searchUserDetails(userDTO);
		return userList;
	}
	
	public UserDTO deleteUser(Long id) throws Exception {
		User user = userRepository.findUserDistinctById(id);
		user.setEnabled("N");
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setUpdatdDtTm(timestamp);
		return ConvertUtil.convertUserObjToUserDTO(userRepository.save(user));
	}
	
	public UserDTO updateUser(UserDTO userDTO) throws Exception {
		if(userDTO.getId() != null && !"".equalsIgnoreCase(userDTO.getId())){
			User user = userRepository.findUserDistinctById(Long.parseLong(userDTO.getId()));
			user =  ConvertUtil.convertUserDTOToUserObj(user,userDTO);
			Timestamp timestamp = new Timestamp(new Date().getTime());
			user.setUpdatdDtTm(timestamp);
			return ConvertUtil.convertUserObjToUserDTO(userRepository.save(user));
		}
		return null;
	}
}
