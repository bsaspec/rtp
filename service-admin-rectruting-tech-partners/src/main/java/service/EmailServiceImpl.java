package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


@Service("emailService")
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	@Qualifier("javaMailSender")
    private JavaMailSender javaMailSender;
	

	
	public void sendMail(String toEmail,String userId, String name) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(toEmail);
            mailMessage.setFrom("rptuser34@gmail.com");
            mailMessage.setSubject("Activate Company");
            mailMessage.setText("please activate the company, by clicking on the below link: http://localhost:8092/newUser?"
            		+ "userid="+userId+"&username="+name+"&psw='dGVtcA=='");
            javaMailSender.send(mailMessage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {}
        
        //return helper;
    }
	
	
}


