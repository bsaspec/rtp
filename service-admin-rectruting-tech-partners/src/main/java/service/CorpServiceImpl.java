package service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import config.ConvertUtil;
import dao.CorpDao;
import domain.Corp;
import domain.User;
import domain.UserRole;
import dto.CorpDTO;
import repository.CorpRepository;
import repository.UserRepository;


@Service("corpService")
@Transactional
public class CorpServiceImpl implements CorpService{
	
	@Autowired
	CorpRepository corpRepository;

	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CorpDao corpDao;
	
	@Transactional
	public CorpDTO saveCorp(CorpDTO corpDTO) throws Exception{
		Corp finalCorp = ConvertUtil.convertCORPDTOToCorpObj(corpDTO);
		User user = ConvertUtil.getUserObject(corpDTO);
		UserRole userRole = ConvertUtil.getUserRoleObject();
		finalCorp.setUsers(null);

		Timestamp timestamp = new Timestamp(new Date().getTime());
		finalCorp.setCreatedDtTm(timestamp);
		finalCorp.setUpdatdDtTm(timestamp);
		System.out.println("corpDTO :" + corpDTO);
		if(corpDTO.getId() != null && !"".equalsIgnoreCase(corpDTO.getId())) {
			finalCorp = corpRepository.findCorpDistinctById(Long.parseLong(corpDTO.getId())); 
		}
		Corp dbCorp =  corpRepository.save(finalCorp);
		Map<Corp, UserRole> map=new HashMap<Corp, UserRole>();
		map.put(dbCorp, userRole);

		user.setCorpRoles(map);
		System.out.println(user);
		user=userRepository.save(user);
		return ConvertUtil.convertCorpObjToCORPDTO(finalCorp,user);

	}
	
	public Corp findFtinNumber(Long ftinNumber) throws Exception{
		return corpRepository.findCorpDistinctByFtinNumber(ftinNumber);
	}


	public List<CorpDTO> findAllCorps() throws Exception { 
		List<CorpDTO> corpDTOsList = null;
		List<Corp> corpList =  corpRepository.getCorps();
		if(corpList != null && corpList.size() > 0){
			corpDTOsList = ConvertUtil.convertCorpListToCORPDTOList(corpList);
		}
		return corpDTOsList;
	}

	public List<CorpDTO> searchCorpDetails(String companyName, String ftinNumber) throws Exception {
		return  corpDao.searchCompanyDetails(companyName,ftinNumber);
	}

	public CorpDTO deleteCorp(Long id) throws Exception {
		Corp corp = corpRepository.findCorpDistinctById(id);
		corp.setEnabled("N");
		Timestamp timestamp = new Timestamp(new Date().getTime());
		corp.setUpdatdDtTm(timestamp);
		return ConvertUtil.convertCorpObjToCORPDTO(corpRepository.save(corp),null);
	}

	public CorpDTO updateCorp(CorpDTO corpDTO) throws Exception {
		if(corpDTO.getId() != null && !"".equalsIgnoreCase(corpDTO.getId())){
			Corp corp = corpRepository.findCorpDistinctById(Long.parseLong(corpDTO.getId()));
			corp =  ConvertUtil.convertCORPDTOToCorpObj(corpDTO,corp);
			return ConvertUtil.convertCorpObjToCORPDTO(corpRepository.save(corp),null);
		}
		return null;
	}
	
	public List<CorpDTO> findAllActiveCompanies() throws Exception { 
		List<CorpDTO> corpDTOsList = null;
		List<Corp> corpList =  corpRepository.getCorps();
		if(corpList != null && corpList.size() > 0){
			corpDTOsList = ConvertUtil.convertActiveCorpListToActiveCORPDTOList(corpList);
		}
		return corpDTOsList;
	}

	public CorpDTO getCorp(Long id) {
		return ConvertUtil.convertCorpObjToCORPDTO(corpRepository.findCorpDistinctById(id),null);
	}

}


