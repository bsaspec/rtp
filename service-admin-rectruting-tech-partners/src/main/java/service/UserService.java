package service;

import java.util.List;

import domain.User;
import dto.UserDTO;

public interface UserService {
	
	UserDTO saveUser(UserDTO userDTO) throws Exception;
	public User findUserDistinctByUserName(String userName)throws Exception;
	public List<UserDTO> findAllUsers() throws Exception ;
	List<UserDTO> searchUserDetails(UserDTO userDTO) throws Exception;
	public UserDTO deleteUser(Long id) throws Exception;
	public UserDTO updateUser(UserDTO userDTO) throws Exception;
}
