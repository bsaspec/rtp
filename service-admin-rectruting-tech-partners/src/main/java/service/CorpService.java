package service;

import java.util.List;

import domain.Corp;
import dto.CorpDTO;

public interface CorpService {
	public CorpDTO saveCorp(CorpDTO corpDTO) throws Exception;
	public Corp findFtinNumber(Long ftinNumber) throws Exception;
	public List<CorpDTO> findAllCorps() throws Exception;
	public List<CorpDTO> searchCorpDetails(String companyName,String ftinNumber) throws Exception;
	public CorpDTO deleteCorp(Long id) throws Exception;
	public CorpDTO updateCorp(CorpDTO corpDTO) throws Exception;
	public CorpDTO getCorp(Long id) throws Exception;
	public List<CorpDTO> findAllActiveCompanies() throws Exception;
}
