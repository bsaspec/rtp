package service;

import java.util.List;

import domain.Recruiter;
import dto.RecruiterDTO;

public interface RecruiterService {
	public RecruiterDTO saveRecruiter(RecruiterDTO recruiterDTO) throws Exception;
	public Recruiter findRecruiterDistinctById(Long id) throws Exception;
	public Recruiter findRecruiterDistinctByRecruiterName(String recruiterName) throws Exception;
	public List<RecruiterDTO> findAllRecruiters() throws Exception;
	public List<RecruiterDTO> searchRecruiterDetails(String recruiterName,String ftinNumber) throws Exception;
	public RecruiterDTO deleteRecruiter(Long id) throws Exception;
	public RecruiterDTO updateRecruiter(RecruiterDTO recruiterDTO) throws Exception;
	public List<RecruiterDTO> findAllActiveRecruiters() throws Exception;
	public RecruiterDTO getRecruiter(Long id) throws Exception;
}
