package service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.UserDao;
import dao.UserSecurityQuestionDao;
import domain.SecurityQuestionMaster;
import domain.User;
import domain.UserSecurityQuestion;
import dto.SecurityQuestionMasterDto;
import dto.SecurityQuestionWrapper;
import dto.StatusCodesDTO;
import dto.UserDTO;
import dto.UserSecurityQuestionDto;
import repository.SecurityQuestionsRepository;
import repository.UserRepository;
import repository.UserSecurityQuestionRepository;

@Service("loginService")
@Transactional
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	SecurityQuestionsRepository securityQuestionMaster;
	
	
	@Autowired
	UserSecurityQuestionRepository userSecurityQuestionRepository;
	
	@Autowired
	UserDao loginDao;
	
	@Autowired
	UserSecurityQuestionDao userSecurityQuestionDao;
	
	public UserDTO loginUser(String userName,String password)throws Exception{
		String pwd =  new String(Base64.encodeBase64(password.getBytes()));
		User user = userRepository.findUserDistinctByUserNameAndPassword(userName, pwd);
		if(user != null){
			UserDTO userDTO = loginDao.getUserDetails(user.getId());
			return userDTO;
		}else{
			return new UserDTO();
		}
		
	}

	@Override
	public List<SecurityQuestionMasterDto> getSecurityQuestions() {
		List<SecurityQuestionMasterDto> securityQuestionMasterDtos=new ArrayList<SecurityQuestionMasterDto>(0);
			List<SecurityQuestionMaster> questionMasters=securityQuestionMaster.getSecurityQuestionMaster();
		 if(!(questionMasters.isEmpty())){
			 
			 for (SecurityQuestionMaster securityQuestionMaster : questionMasters) {
				 SecurityQuestionMasterDto questionMasterDto=new SecurityQuestionMasterDto();
				 BeanUtils.copyProperties(securityQuestionMaster, questionMasterDto);
				 securityQuestionMasterDtos.add(questionMasterDto);
				
			}
		 }
		 return securityQuestionMasterDtos;
		
	}

	@Override
	public List<UserSecurityQuestionDto> getCheckUser(Long userId) {
		
		List<UserSecurityQuestionDto> questionDtos=new ArrayList<>();
		List<UserSecurityQuestion> list=userSecurityQuestionDao.getAllAnswers(userId);
		if(!(list.isEmpty())){
			for (UserSecurityQuestion userSecurityQuestion : list) {
				
				UserSecurityQuestionDto dto=new UserSecurityQuestionDto();
				dto.setQuestion_id(userSecurityQuestion.getSecurityQuestionMaster().getQuestionId());
				dto.setAnswers(userSecurityQuestion.getAnswer());
				questionDtos.add(dto);
				
			}
			
			
		}
		
		return questionDtos;
	}
	
	/*
	 * @ Author Rajiv
	 * 
	 * */
	
	@Override
	public Object saveAnswers(SecurityQuestionWrapper securityQuestionWrapper) {
		UserSecurityQuestion userSecurityQuestion=new UserSecurityQuestion();
		StatusCodesDTO codesDTO=new StatusCodesDTO();
		
		boolean insertStatus=false;
		try {
			User user=userRepository.findOne(securityQuestionWrapper.getUserId());
			user.setEnabled("Y");
			String pwd =  new String(Base64.encodeBase64(securityQuestionWrapper.getPassword().getBytes()));
			user.setPassword(pwd);
			userRepository.save(user);
			insertStatus=true;
			
		} catch (Exception exception) {
			insertStatus=false;
			exception.printStackTrace();
			System.out.println("** No User With This User ID **"+exception.getCause());
			codesDTO.setStatusCode(500);
			codesDTO.setStatusMessage("Error while inserting"+exception.getCause());
			return new ResponseEntity<StatusCodesDTO>(codesDTO,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(insertStatus) {
			try {
				for (SecurityQuestionMasterDto securityQuestionMasterDto : securityQuestionWrapper.getUserCheckins()) {
					
					User user=new User();
					user.setId(securityQuestionWrapper.getUserId());
					SecurityQuestionMaster questionMaster=new SecurityQuestionMaster();
					questionMaster.setQuestionId(securityQuestionMasterDto.getQuestionId());
					userSecurityQuestion.setUser(user);
					userSecurityQuestion.setSecurityQuestionMaster(questionMaster);
					userSecurityQuestion.setAnswer(securityQuestionMasterDto.getAnswer());
					userSecurityQuestionRepository.save(userSecurityQuestion);
					
				}
				codesDTO.setStatusCode(200);
				codesDTO.setStatusMessage("OK");
				return new ResponseEntity<StatusCodesDTO>(codesDTO,HttpStatus.OK);
				
			} catch (Exception e) {
				codesDTO.setStatusCode(500);
				codesDTO.setStatusMessage("Error while saving security questions "+e.getCause());
				return new ResponseEntity<StatusCodesDTO>(codesDTO,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
		}
		return insertStatus;
		
	}
}
