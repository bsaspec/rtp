package service;

public interface EmailService {
	public void sendMail(String toEmail, String userId, String name);
}
