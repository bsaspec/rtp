package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import config.Constants;
import domain.Corp;
import dto.RecruiterDTO;


@Repository("recruiterDao")
public class RecruiterDaoImpl  implements RecruiterDao {

	@PersistenceContext
	EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<RecruiterDTO> searchRecruiterDetails(String recruiterName,Corp corp) throws Exception {
		List<RecruiterDTO> results = null;

		StringBuffer hql = new StringBuffer("");
		Session session = (Session)this.entityManager.getDelegate();
		hql.append(Constants.RECRUITER_DETAILS);
		
		if((recruiterName != null && !"".equalsIgnoreCase(recruiterName)) || (corp != null)){
			hql.append(" and ");
		}

		if(recruiterName != null && !"".equalsIgnoreCase(recruiterName)){
			hql.append("name = "+"'"+recruiterName+"'");
		}

		if(corp != null){
			if(recruiterName != null && !"".equalsIgnoreCase(recruiterName)){
				hql.append(" and ");
			}
			hql.append("corp_id = "+"'"+corp.getId()+"'");
		}
		System.out.println("query--->"+hql.toString());
		results =  session.createSQLQuery(hql.toString()).
				addScalar("id",new StringType()).
				addScalar("recruiterName",new StringType()).
				addScalar("firstName",new StringType()).
				addScalar("middleName",new StringType()).
				addScalar("lastName",new StringType()).
				addScalar("joinDate",new StringType()).
				addScalar("activeFlag",new StringType()).
				addScalar("personalEmail",new StringType()).
				addScalar("officialEmail",new StringType()).
				addScalar("birthDate",new StringType()).
				addScalar("highestQualifincation",new StringType()).
				addScalar("passDate",new StringType()).
				addScalar("recrutingComments",new StringType()).
				addScalar("salary",new StringType()).
				addScalar("commission",new StringType()).
				addScalar("prevExp",new StringType()).
				addScalar("prevCompWorked",new StringType()).
				addScalar("nativePlace",new StringType()).
				addScalar("addrLine1",new StringType()).
				addScalar("addrLine2",new StringType()).
				addScalar("addrLine3",new StringType()).
				addScalar("town",new StringType()).
				addScalar("district",new StringType()).
				addScalar("state",new StringType()).
				addScalar("zipcode",new StringType()).
				addScalar("permAddrLine1",new StringType()).
				addScalar("permAddrLine2",new StringType()).
				addScalar("permAddrLine3",new StringType()).
				addScalar("permtTown",new StringType()).
				addScalar("permDistrict",new StringType()).
				addScalar("permState",new StringType()).
				addScalar("permZipcode",new StringType()).
				setResultTransformer(new AliasToBeanResultTransformer(RecruiterDTO.class)).list();
		System.out.println("RecruiterDTO result-->"+results.size());
		return results;
	}


}