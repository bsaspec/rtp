package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import config.Constants;
import dto.CorpDTO;
import dto.UserDTO;

@Repository("loginDao")
public class UserDaoImpl  implements UserDao {

	@PersistenceContext
	EntityManager entityManager;



	@SuppressWarnings({"unchecked" })
	public UserDTO getUserDetails(Long userId)throws Exception {
		List<UserDTO> userDTOs = null;
		List<CorpDTO> corpDTOs = null;
		UserDTO userDTO= null;
		StringBuffer hql = new StringBuffer("");
		Session session = (Session)this.entityManager.getDelegate();
		hql.append(Constants.USER_QUERY);
		hql.append(" user.id = "+"'"+userId+"'");

		userDTOs =  session.createSQLQuery(hql.toString()).
				addScalar("userId",new StringType()).
				addScalar("userName",new StringType()).
				addScalar("enabled",new StringType()).
				addScalar("email",new StringType()).
				addScalar("roleId",new StringType()).
				addScalar("role",new StringType()).
				addScalar("type",new StringType()).
				setResultTransformer(new AliasToBeanResultTransformer(UserDTO.class)).list();

		if(userDTOs != null && userDTOs.size() > 0){
			userDTO = userDTOs.get(0);
			hql = new StringBuffer("");
			hql.append(Constants.COMPANY_ROLES_DETAILS);
			hql.append("  usrCorpRole.user_id = "+userId);

			corpDTOs =  session.createSQLQuery(hql.toString()).
					addScalar("id",new StringType()).
					addScalar("companyName",new StringType()).
					addScalar("addressLine1",new StringType()).
					addScalar("addressLine2",new StringType()).
					addScalar("city",new StringType()).
					addScalar("state",new StringType()).
					addScalar("zip",new StringType()).
					addScalar("eodAlertEmail",new StringType()).
					addScalar("interviewIntegrationEmail",new StringType()).
					addScalar("ftinNumber",new StringType()).
					addScalar("signingAuthorityFirstName",new StringType()).
					addScalar("signingAuthorityMiddleName",new StringType()).
					addScalar("signingAuthorityLastName",new StringType()).
					addScalar("signingAuthorityPhone",new StringType()).
					addScalar("signingAuthorityDesignation",new StringType()).
					addScalar("signingAuthorityEmail",new StringType()).
					addScalar("ownerFirstName",new StringType()).
					addScalar("ownerMiddleName",new StringType()).
					addScalar("ownerLastName",new StringType()).
					addScalar("ownerPhone",new StringType()).
					addScalar("ownerAlternatePhone",new StringType()).
					addScalar("invoiceContactFirstName",new StringType()).
					addScalar("invoiceContactMiddleName",new StringType()).
					addScalar("invoiceContactLastName",new StringType()).
					addScalar("invoiceContactPhone",new StringType()).
					addScalar("invoiceContactEmail",new StringType()).
					addScalar("portalAdminFirstName",new StringType()).
					addScalar("portalAdminMiddleName",new StringType()).
					addScalar("portalAdminLastName",new StringType()).
					addScalar("portalAdminPhone",new StringType()).
					addScalar("portalAdminEmail",new StringType()).
					addScalar("invoiceContactPhone",new StringType()).
					addScalar("invoiceContactEmail",new StringType()).
					addScalar("portalAdminFirstName",new StringType()).
					addScalar("portalAdminMiddleName",new StringType()).
					addScalar("enabled",new StringType()).
					setResultTransformer(new AliasToBeanResultTransformer(CorpDTO.class)).list();
			System.out.println("results1111-->"+corpDTOs.size());

			if(corpDTOs != null && corpDTOs.size() > 0){
				userDTO.setCorpDTOList(corpDTOs);
			}
		}
		return userDTO;
	}	

	public UserDTO getRole(UserDTO userDTO){
		/*Criteria cr = sessionFactory.getCurrentSession().createCriteria(UserRole.class);
	cr.add(Restrictions.eq("id", userDTO.getRoleId()));
	List userRoleList = cr.list();
	if(userRoleList.size() > 0){
		for (Iterator iterator = 
				userRoleList.iterator(); iterator.hasNext();){
			UserRole userRole = (UserRole) iterator.next(); 
			userDTO.setRole(userRole.getRole());
			userDTO.setType(userRole.getType());
		}
	}
		 */	return new UserDTO();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getQuestions() {
		Session session = (Session)this.entityManager.getDelegate();
		Query qry = session.createQuery("select p.productId,p.proName from Product p");

		List l =qry.list();
		System.out.println("Total Number Of Records : "+l.size());

	}	

	@SuppressWarnings({"unchecked" })
	public List<UserDTO> searchUserDetails(UserDTO userDTO)throws Exception {
		List<UserDTO> userDTOs = null;
		StringBuffer hql = new StringBuffer("");
		Session session = (Session)this.entityManager.getDelegate();
		hql.append(Constants.USER_SEARCH_QUERY);

		if((userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName())) || 
				(userDTO.getFirstName() != null && !"".equalsIgnoreCase(userDTO.getFirstName())) ||
				(userDTO.getLastName() != null && !"".equalsIgnoreCase(userDTO.getLastName())) ||
				(userDTO.getEnabled() != null && !"".equalsIgnoreCase(userDTO.getEnabled()) && !"All".equalsIgnoreCase(userDTO.getEnabled()))){
			hql.append(" and ");
		}

		if(userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName())){
			hql.append(" user_name = "+"'"+userDTO.getUserName()+"'");
		}

		if(userDTO.getFirstName() != null && !"".equalsIgnoreCase(userDTO.getFirstName())){
			if(userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName())){
				hql.append(" and ");
			}
			hql.append(" first_name = "+"'"+userDTO.getFirstName()+"'");
		}

		if(userDTO.getLastName() != null && !"".equalsIgnoreCase(userDTO.getLastName())){
			if(
					(userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName())) 
					|| 
					(userDTO.getFirstName() != null && !"".equalsIgnoreCase(userDTO.getFirstName()))
					){
				hql.append(" and ");
			}
			hql.append(" last_name = "+"'"+userDTO.getLastName()+"'");
		}

		if(userDTO.getEnabled() != null && !"".equalsIgnoreCase(userDTO.getEnabled()) && !"All".equalsIgnoreCase(userDTO.getEnabled())){
			if(
					(userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName()))
					||
					(userDTO.getFirstName() != null && !"".equalsIgnoreCase(userDTO.getFirstName()))
					||
					(userDTO.getLastName() != null && !"".equalsIgnoreCase(userDTO.getLastName()))){
				hql.append(" and ");
			}
			hql.append(" enable_flg = "+"'"+userDTO.getEnabled()+"'");
		}

		userDTOs =  session.createSQLQuery(hql.toString()).
				addScalar("userId",new StringType()).
				addScalar("userName",new StringType()).
				addScalar("enabled",new StringType()).
				addScalar("email",new StringType()).
				addScalar("roleId",new StringType()).
				addScalar("role",new StringType()).
				addScalar("type",new StringType()).
				addScalar("lastName",new StringType()).
				addScalar("firstName",new StringType()).
				setResultTransformer(new AliasToBeanResultTransformer(UserDTO.class)).list();
		return userDTOs;
	}	

}
