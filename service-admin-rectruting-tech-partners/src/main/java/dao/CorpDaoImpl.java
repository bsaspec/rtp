package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import config.Constants;
import dto.CorpDTO;


@Repository("corpDao")
public class CorpDaoImpl  implements CorpDao {

	@PersistenceContext
	EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<CorpDTO> searchCompanyDetails(String companyName, String ftinNumber) throws Exception {
		List<CorpDTO> results = null;

		StringBuffer hql = new StringBuffer("");
		Session session = (Session)this.entityManager.getDelegate();
		hql.append(Constants.COMPANY_DETAILS);
		
		if((companyName != null && !"".equalsIgnoreCase(companyName)) || (ftinNumber != null && !"".equalsIgnoreCase(ftinNumber))){
			hql.append(" where ");
		}

		if(companyName != null && !"".equalsIgnoreCase(companyName)){
			hql.append("name = "+"'"+companyName+"'");
		}

		if(ftinNumber != null && !"".equalsIgnoreCase(ftinNumber)){
			if(companyName != null && !"".equalsIgnoreCase(companyName)){
				hql.append(" and ");
			}
			hql.append("ftin_number = "+"'"+ftinNumber+"'");
		}
		System.out.println("query--->"+hql.toString() + ftinNumber);
		results =  session.createSQLQuery(hql.toString()).
				addScalar("id",new StringType()).
				addScalar("companyName",new StringType()).
				addScalar("addressLine1",new StringType()).
				addScalar("addressLine2",new StringType()).
				addScalar("city",new StringType()).
				addScalar("state",new StringType()).
				addScalar("zip",new StringType()).
				addScalar("eodAlertEmail",new StringType()).
				addScalar("interviewIntegrationEmail",new StringType()).
				addScalar("ftinNumber",new StringType()).
				addScalar("signingAuthorityFirstName",new StringType()).
				addScalar("signingAuthorityMiddleName",new StringType()).
				addScalar("signingAuthorityLastName",new StringType()).
				addScalar("signingAuthorityPhone",new StringType()).
				addScalar("signingAuthorityDesignation",new StringType()).
				addScalar("signingAuthorityEmail",new StringType()).
				addScalar("ownerFirstName",new StringType()).
				addScalar("ownerMiddleName",new StringType()).
				addScalar("ownerLastName",new StringType()).
				addScalar("ownerPhone",new StringType()).
				addScalar("ownerAlternatePhone",new StringType()).
				addScalar("invoiceContactFirstName",new StringType()).
				addScalar("invoiceContactMiddleName",new StringType()).
				addScalar("invoiceContactLastName",new StringType()).
				addScalar("invoiceContactPhone",new StringType()).
				addScalar("invoiceContactEmail",new StringType()).
				addScalar("portalAdminFirstName",new StringType()).
				addScalar("portalAdminMiddleName",new StringType()).
				addScalar("portalAdminLastName",new StringType()).
				addScalar("portalAdminPhone",new StringType()).
				addScalar("portalAdminEmail",new StringType()).
				addScalar("invoiceContactPhone",new StringType()).
				addScalar("invoiceContactEmail",new StringType()).
				addScalar("portalAdminFirstName",new StringType()).
				addScalar("portalAdminMiddleName",new StringType()).
				addScalar("enabled",new StringType()).
				setResultTransformer(new AliasToBeanResultTransformer(CorpDTO.class)).list();
		System.out.println("company result-->"+results.size());
		return results;
	}


}