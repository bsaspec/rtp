package dao;

import java.util.List;

import dto.CorpDTO;


public interface CorpDao {
	public List<CorpDTO> searchCompanyDetails(String companyName,String ftinNumber) throws Exception;
}
