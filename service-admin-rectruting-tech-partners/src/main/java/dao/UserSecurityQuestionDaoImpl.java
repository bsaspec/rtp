package dao;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import domain.UserSecurityQuestion;

@Repository("userSecurityQuestionDao")
public class UserSecurityQuestionDaoImpl implements UserSecurityQuestionDao {
	
	
	@PersistenceContext
	EntityManager entityManager;

	

	@SuppressWarnings("unused")
	@Override
	public Boolean getCheckUser(Long questionId, Long userId, String answer) {
		Session session=(Session)this.entityManager.getDelegate();
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<UserSecurityQuestion> getAllAnswers(Long userId) {
		Session session=(Session)this.entityManager.getDelegate();
		Criteria criteria=session.createCriteria(UserSecurityQuestion.class,"UserSecurityQuestion")
				.createAlias("UserSecurityQuestion.user", "user")
				.add(Restrictions.eq("user.id", userId));
		return criteria.list();
	}

}
