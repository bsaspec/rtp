package dao;

import java.util.List;

import dto.UserDTO;

public interface UserDao {
	UserDTO getUserDetails(Long userId) throws Exception;
	UserDTO getRole(UserDTO userDTO);
	void getQuestions();
	public List<UserDTO> searchUserDetails(UserDTO userDTO)throws Exception;
}
