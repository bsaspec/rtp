package dao;

import java.util.List;

import domain.UserSecurityQuestion;

public interface UserSecurityQuestionDao {
	
	Boolean getCheckUser(Long questionId, Long userId, String answer);

	List<UserSecurityQuestion> getAllAnswers(Long userId);

}
