package dao;

import java.util.List;

import domain.Corp;
import dto.RecruiterDTO;


public interface RecruiterDao {
	public List<RecruiterDTO> searchRecruiterDetails(String recruiterName,Corp corp) throws Exception;
}
