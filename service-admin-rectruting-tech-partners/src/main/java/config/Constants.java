package config;

public class Constants {
	public static String USER_QUERY = "SELECT user.id as userId, user.user_name as userName,user.enable_flg as enabled,user.email as email,roleMaster.id as roleId,roleMaster.role as role,roleMaster.type as type FROM usr user,usr_corp_role usrCorpRole, role_master roleMaster where user.id = usrCorpRole.user_id and usrCorpRole.role_id = roleMaster.id and ";
	public static String USER_SEARCH_QUERY = "SELECT user.id as userId, user.user_name as userName,user.enable_flg as enabled,user.email as email,roleMaster.id as roleId,roleMaster.role as role,roleMaster.type as type,user.last_name as lastName,user.first_name as firstName FROM usr user,usr_corp_role usrCorpRole, role_master roleMaster where user.id = usrCorpRole.user_id and usrCorpRole.role_id = roleMaster.id  ";
	public static String COMPANY_ROLES_DETAILS = "select company.id as id,company.name as companyName,company.addr_line_1 as addressLine1,company.addr_line_2 as addressLine2,company.city as city,company.state as state,company.zip as zip,company.eod_alert_email eodAlertEmail,company.intrvw_integration_email interviewIntegrationEmail,company.ftin_number ftinNumber,company.signing_auth_fname signingAuthorityFirstName,company.signing_auth_mname signingAuthorityMiddleName,company.signing_auth_lname signingAuthorityLastName,company.signing_auth_phone signingAuthorityPhone,company.signing_auth_desig signingAuthorityDesignation,company.signing_auth_email signingAuthorityEmail,company.owner_fname ownerFirstName,company.owner_mname ownerMiddleName,company.owner_lname ownerLastName,company.owner_phone ownerPhone, company.owner_alt_phone ownerAlternatePhone,company.invoice_contact_fname invoiceContactFirstName,company.invoice_contact_mname invoiceContactMiddleName,company.invoice_contact_lname invoiceContactLastName,company.invoice_contact_email invoiceContactPhone, company.invoice_contact_phone invoiceContactEmail,company.portal_admin_fname portalAdminFirstName,company.portal_admin_mname portalAdminMiddleName,company.portal_admin_lname portalAdminLastName,company.portal_admin_email portalAdminEmail,company.portal_admin_phone portalAdminPhone,company.enable_flg enabled from corp company, usr_corp_role usrCorpRole where company.id = usrCorpRole.corp_id and ";
	public static String COMPANY_DETAILS = "select company.id as id,company.name as companyName,company.addr_line_1 as addressLine1,company.addr_line_2 as addressLine2,company.city as city,company.state as state,company.zip as zip,company.eod_alert_email eodAlertEmail,company.intrvw_integration_email interviewIntegrationEmail,"
			+ "company.ftin_number ftinNumber,company.signing_auth_fname signingAuthorityFirstName,company.signing_auth_mname signingAuthorityMiddleName,"
			+"company.signing_auth_lname signingAuthorityLastName,company.signing_auth_phone signingAuthorityPhone,"
			+"company.signing_auth_desig signingAuthorityDesignation,company.signing_auth_email signingAuthorityEmail,company.owner_fname ownerFirstName,company.owner_mname ownerMiddleName,company.owner_lname ownerLastName,company.owner_phone ownerPhone, "
			+"company.owner_alt_phone ownerAlternatePhone,company.invoice_contact_fname invoiceContactFirstName,company.invoice_contact_mname invoiceContactMiddleName,company.invoice_contact_lname invoiceContactLastName,company.invoice_contact_email invoiceContactPhone, "
			+"company.invoice_contact_phone invoiceContactEmail,company.portal_admin_fname portalAdminFirstName,company.portal_admin_mname portalAdminMiddleName,company.portal_admin_lname portalAdminLastName,company.portal_admin_email portalAdminEmail,company.portal_admin_phone portalAdminPhone,company.enable_flg enabled "
			+" from newworld.corp company ";
	public static String N = "N";
	public static String TEMP_PASSWORD = "temp";
	public static String ADMIN = "Admin";
	public static String RECRUITER = "Recruiter";
	public static String Y = "Y";
	public static String NULL_SPACE = "";
	public static String DATE_FORMAT = "dd-MM-yyyy";
	public static String RECRUITER_DETAILS = "SELECT recr.id as id,name recruiterName, first_name as firstName,middle_name as middleName,last_name as lastName,"
											 +"join_date as joinDate,enable_flg as activeFlag,personal_email as personalEmail,official_email as officialEmail,"
											 +"birth_date as birthDate,highest_qualification as highestQualifincation,pass_date as passDate,"
											 +"recruting_comments as recrutingComments,salary as salary,commission as commission,prev_exp as prevExp,"
											 +"prev_comp_worked as prevCompWorked,native_place as nativePlace,addr_line_1 as addrLine1,"
											 +"addr_line_2 as addrLine2,addr_line_3 as addrLine3,town as town,district as district,"
											 +"state as state,zipcode as zipcode,perm_addr_line1 as permAddrLine1,perm_addr_line2 as permAddrLine2,"
											 +"perm_addr_line3 as permAddrLine3,perm_town as permtTown,perm_district as permDistrict,"
											 +"perm_state as permState,perm_zipcode as permZipcode from recruiter recr, usr_corp_role usrCorpRole "
											 +"where recr.user_id = usrCorpRole.user_id ";
}
