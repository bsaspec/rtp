package config;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import domain.Corp;
import domain.Recruiter;
import domain.User;
import domain.UserRole;
import dto.CorpDTO;
import dto.RecruiterDTO;
import dto.UserDTO;

public class ConvertUtil {
	


	public static List<UserDTO> convertUserListToUserDTOList(List<User> userList){
		List<UserDTO> userDTOList = new ArrayList<UserDTO>();
		for (User user : userList) {
			UserDTO userDTO = convertUserObjToUserDTO(user);
			if(userDTO.getEnabled() != null && !Constants.NULL_SPACE.equalsIgnoreCase(userDTO.getEnabled()) && Constants.Y.equalsIgnoreCase(userDTO.getEnabled())){
				userDTOList.add(userDTO);
			}
		}
		return userDTOList;
	}
	
	public static UserDTO convertUserObjToUserDTO(User user){
		UserDTO userDTO = new UserDTO();
		userDTO.setId(Long.toString(user.getId()));
		userDTO.setUserName(user.getUserName());
		userDTO.setEmail(user.getEmail());

		userDTO.setLastName(user.getLastName());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setEmail(user.getEmail());
		userDTO.setEnabled(user.getEnabled());

		return userDTO;
	}
	
	public static User convertUserDTOToUserObj(UserDTO userDTO,User dbUser){
		User user = new User();
		user.setUserName(userDTO.getUserName());
		String pwd =  new String(Base64.encodeBase64(userDTO.getPassword().getBytes()));
		user.setPassword(pwd);
		user.setEmail(userDTO.getEmail());
		user.setEnabled(Constants.Y);
		user.setLastName(userDTO.getLastName());
		user.setFirstName(userDTO.getFirstName());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setCreatedDtTm(timestamp);
		user.setUpdatdDtTm(timestamp);
		user.setCreatedByUserId(dbUser.getUserName());
		user.setUpdatedByUserId(dbUser.getUserName());

		return user;
	}
	
	public static User convertUserDTOToUserObj(User user,UserDTO userDTO){
		if(userDTO.getUserName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(userDTO.getUserName())){
			user.setUserName(userDTO.getUserName());
		}
		if(userDTO.getEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(userDTO.getEmail())){
			user.setEmail(userDTO.getEmail());
		}
		if(userDTO.getLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(userDTO.getLastName())){
			user.setLastName(userDTO.getLastName());
		}
		if(userDTO.getFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(userDTO.getFirstName())){
			user.setFirstName(userDTO.getFirstName());
		}
		user.setUpdatedByUserId(userDTO.getUserId());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setUpdatdDtTm(timestamp);
		return user;
	}
	
	public static Corp convertCORPDTOToCorpObj(CorpDTO corpDTO){
		Corp corp = new Corp();
		if(corpDTO.getId() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getId())){
			corp.setId(Long.parseLong(corpDTO.getId()));
		}
		corp.setCompanyName(corpDTO.getCompanyName());
		corp.setAddressLine1(corpDTO.getAddressLine1());
		corp.setAddressLine2(corpDTO.getAddressLine2());
		corp.setCity(corpDTO.getCity());
		corp.setState(corpDTO.getState());
		corp.setZip(corpDTO.getZip());
		corp.setEodAlertEmail(corpDTO.getEodAlertEmail());
		corp.setInterviewIntegrationEmail(corpDTO.getInterviewIntegrationEmail());
		corp.setSigningAuthorityFirstName(corpDTO.getSigningAuthorityFirstName());
		corp.setSigningAuthorityMiddleName(corpDTO.getSigningAuthorityMiddleName());
		corp.setSigningAuthorityLastName(corpDTO.getSigningAuthorityLastName());
		corp.setSigningAuthorityPhone(corpDTO.getSigningAuthorityPhone());
		corp.setSigningAuthorityDesignation(corpDTO.getSigningAuthorityDesignation());
		corp.setSigningAuthorityEmail(corpDTO.getSigningAuthorityEmail());
		corp.setOwnerFirstName(corpDTO.getOwnerFirstName());
		corp.setOwnerMiddleName(corpDTO.getOwnerMiddleName());
		corp.setOwnerLastName(corpDTO.getOwnerLastName());
		corp.setOwnerPhone(corpDTO.getOwnerPhone());
		corp.setOwnerAlternatePhone(corpDTO.getOwnerAlternatePhone());
		corp.setInvoiceContactFirstName(corpDTO.getInvoiceContactFirstName());
		corp.setInvoiceContactMiddleName(corpDTO.getInvoiceContactMiddleName());
		corp.setInvoiceContactLastName(corpDTO.getInvoiceContactLastName());
		corp.setInvoiceContactPhone(corpDTO.getInvoiceContactPhone());
		corp.setFtinNumber(Long.parseLong(corpDTO.getFtinNumber()));
		corp.setInvoiceContactEmail(corpDTO.getInvoiceContactEmail());
		corp.setPortalAdminFirstName(corpDTO.getPortalAdminFirstName());
		corp.setPortalAdminMiddleName(corpDTO.getPortalAdminMiddleName());
		corp.setPortalAdminLastName(corpDTO.getPortalAdminLastName());
		corp.setPortalAdminPhone(corpDTO.getPortalAdminPhone());

		corp.setPortalAdminEmail(corpDTO.getPortalAdminEmail());
		corp.setEnabled(Constants.Y);

		corp.setCreatedByUserId(corpDTO.getUserId());
		corp.setUpdatedByUserId(corpDTO.getUserId());
		return corp;
	}

	public static User getUserObject(CorpDTO corpDTO){
		User user = new User();
		user.setUserName(corpDTO.getPortalAdminEmail());
		user.setEmail(corpDTO.getPortalAdminEmail());
		user.setEnabled(Constants.N);
		String password = Constants.TEMP_PASSWORD;
		String pwd =  new String(Base64.encodeBase64(password.getBytes()));
		user.setPassword(pwd);
		user.setFirstName(corpDTO.getPortalAdminFirstName());
		user.setLastName(corpDTO.getPortalAdminLastName());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setCreatedDtTm(timestamp);
		user.setUpdatdDtTm(timestamp);
		user.setCreatedByUserId(corpDTO.getUserId());
		user.setUpdatedByUserId(corpDTO.getUserId());
		return user;
	}

	public static User getUserObjectForRecruiter(RecruiterDTO recruiterDTO){
		User user = new User();
		user.setUserName(recruiterDTO.getPersonalEmail());
		user.setEmail(recruiterDTO.getPersonalEmail());
		user.setEnabled(Constants.N);
		String password = Constants.TEMP_PASSWORD;
		String pwd =  new String(Base64.encodeBase64(password.getBytes()));
		user.setPassword(pwd);
		user.setFirstName(recruiterDTO.getFirstName());
		user.setLastName(recruiterDTO.getLastName());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setCreatedDtTm(timestamp);
		user.setUpdatdDtTm(timestamp);
		user.setCreatedByUserId(recruiterDTO.getUserId());
		user.setUpdatedByUserId(recruiterDTO.getUserId());
		return user;
	}

	public static UserRole getUserRoleObject(){
		UserRole userRole=new UserRole();
		userRole.setId(1l);
		userRole.setType(Constants.ADMIN);
		userRole.setRole(Constants.ADMIN);
		return userRole;
	}

	public static UserRole getUserRoleObjectForRecruiter(){
		UserRole userRole=new UserRole();
		userRole.setId(3l);
		userRole.setType(Constants.RECRUITER);
		userRole.setRole(Constants.RECRUITER);
		return userRole;
	}

	public static CorpDTO convertCorpObjToCORPDTO(Corp corp, User user){
		CorpDTO corpDTO = null;
		if(corp != null){
			corpDTO = new CorpDTO();
			corpDTO.setId(Long.toString(corp.getId()));
			corpDTO.setCompanyName(corp.getCompanyName());
			corpDTO.setAddressLine1(corp.getAddressLine1());
			corpDTO.setAddressLine2(corp.getAddressLine2());
			corpDTO.setCity(corp.getCity());
			corpDTO.setState(corp.getState());
			corpDTO.setZip(corp.getZip());
			corpDTO.setEodAlertEmail(corp.getEodAlertEmail());
			corpDTO.setInterviewIntegrationEmail(corp.getInterviewIntegrationEmail());
			corpDTO.setSigningAuthorityFirstName(corp.getSigningAuthorityFirstName());
			corpDTO.setSigningAuthorityMiddleName(corp.getSigningAuthorityMiddleName());
			corpDTO.setSigningAuthorityLastName(corp.getSigningAuthorityLastName());
			corpDTO.setSigningAuthorityPhone(corp.getSigningAuthorityPhone());
			corpDTO.setSigningAuthorityDesignation(corp.getSigningAuthorityDesignation());
			corpDTO.setSigningAuthorityEmail(corp.getSigningAuthorityEmail());
			corpDTO.setOwnerFirstName(corp.getOwnerFirstName());
			corpDTO.setOwnerMiddleName(corp.getOwnerMiddleName());
			corpDTO.setOwnerLastName(corpDTO.getOwnerLastName());
			corpDTO.setOwnerPhone(corp.getOwnerPhone());
			corpDTO.setOwnerAlternatePhone(corp.getOwnerAlternatePhone());
			corpDTO.setInvoiceContactFirstName(corp.getInvoiceContactFirstName());
			corpDTO.setInvoiceContactMiddleName(corp.getInvoiceContactMiddleName());
			corpDTO.setInvoiceContactLastName(corp.getInvoiceContactLastName());
			corpDTO.setInvoiceContactPhone(corp.getInvoiceContactPhone());
			corpDTO.setFtinNumber(String.valueOf(corp.getFtinNumber()));
			corpDTO.setInvoiceContactEmail(corp.getInvoiceContactEmail());
			corpDTO.setPortalAdminFirstName(corp.getPortalAdminFirstName());
			corpDTO.setPortalAdminMiddleName(corpDTO.getPortalAdminMiddleName());
			corpDTO.setPortalAdminLastName(corp.getPortalAdminLastName());
			corpDTO.setPortalAdminPhone(corp.getPortalAdminPhone());
			if(user!=null)
			corpDTO.setUserId(user.getId().toString());
	
			corpDTO.setPortalAdminEmail(corp.getPortalAdminEmail());
			corpDTO.setEnabled(corp.getEnabled());
			corpDTO.setCreatedByUserId(corp.getCreatedByUserId());
			corpDTO.setUpdatedByUserId(corp.getUpdatedByUserId());
		}
		return corpDTO;
	}

	public static List<CorpDTO> convertCorpListToCORPDTOList(List<Corp> corpList){
		List<CorpDTO> corpDTOList = new ArrayList<CorpDTO>();
		for (Corp corp : corpList) {
			CorpDTO corpDTO = convertCorpObjToCORPDTO(corp,null);
			if(corpDTO.getEnabled() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getEnabled()) && Constants.Y.equalsIgnoreCase(corpDTO.getEnabled())){
				corpDTOList.add(corpDTO);
			}
		}
		return corpDTOList;
	}
	
	public static List<RecruiterDTO> convertRecruiterListToRecruiterDTOList(List<Recruiter> recruiterList){
		List<RecruiterDTO> recruiterDTOList = new ArrayList<RecruiterDTO>();
		for (Recruiter recruiter : recruiterList) {
			RecruiterDTO recruiterDTO = convertRecruiterObjToRecruiterDTO(recruiter);
			recruiterDTOList.add(recruiterDTO);
		}
		return recruiterDTOList;
	}
	
	public static List<RecruiterDTO> convertActiveRecruiterListToActiveRecruiterDTOList(List<Recruiter> recruiterList){
		List<RecruiterDTO> recruiterDTOList = new ArrayList<RecruiterDTO>();
		for (Recruiter recruiter : recruiterList) {
			if(recruiter.getActiveFlag() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiter.getActiveFlag()) && Constants.Y.equalsIgnoreCase(recruiter.getActiveFlag())){
				RecruiterDTO recruiterDTO = convertRecruiterObjToRecruiterDTO(recruiter);
				recruiterDTOList.add(recruiterDTO);
			}
		}
		return recruiterDTOList;
	}
	
	public static List<CorpDTO> convertActiveCorpListToActiveCORPDTOList(List<Corp> corpList){
		List<CorpDTO> corpDTOList = new ArrayList<CorpDTO>();
		for (Corp corp : corpList) {
			if(corp.getEnabled() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corp.getEnabled()) && Constants.Y.equalsIgnoreCase(corp.getEnabled())){
				CorpDTO corpDTO = convertCorpObjToCORPDTO(corp,null);
				corpDTOList.add(corpDTO);
			}
		}
		return corpDTOList;
	}
	

	public static User convertCorpDTOToUserObj(CorpDTO newCorpDTO){
		User user = new User();
		user.setUserName(newCorpDTO.getPortalAdminEmail());
		user.setEmail(newCorpDTO.getPortalAdminEmail());
		user.setEnabled(Constants.N);
		String password = Constants.TEMP_PASSWORD;
		String pwd =  new String(Base64.encodeBase64(password.getBytes()));
		user.setPassword(pwd);
		user.setFirstName(newCorpDTO.getPortalAdminFirstName());
		user.setLastName(newCorpDTO.getPortalAdminLastName());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		user.setCreatedDtTm(timestamp);
		user.setUpdatdDtTm(timestamp);
		user.setCreatedByUserId(newCorpDTO.getCreatedByUserId());
		user.setUpdatedByUserId(newCorpDTO.getUpdatedByUserId());

		/*UserCorpRole userCorpRole = new UserCorpRole();
		userCorpRole.setCorpId(newCorpDTO.getId());
		userCorpRole.setRoleId(Long.parseLong("2"));


		Set<UserCorpRole> userCorpRolesList = new HashSet<UserCorpRole>();
		userCorpRolesList.add(userCorpRole);*/
		//user.setUserCorpRole(userCorpRolesList);
		return user;
	}


	public static Corp convertCORPDTOToCorpObj(CorpDTO corpDTO,Corp corp){
		
		if(corpDTO.getAddressLine1() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getAddressLine1())){
			corp.setAddressLine1(corpDTO.getAddressLine1());
		}
		if(corpDTO.getAddressLine2() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getAddressLine2())){
			corp.setAddressLine2(corpDTO.getAddressLine2());
		}
		if(corpDTO.getCity() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getCity())){
			corp.setCity(corpDTO.getCity());
		}
		if(corpDTO.getState() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getState())){
			corp.setState(corpDTO.getState());
		}
		if(corpDTO.getState() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getState())){
			corp.setState(corpDTO.getState());
		}
		if(corpDTO.getZip() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getZip())){
			corp.setZip(corpDTO.getZip());
		}
		if(corpDTO.getEodAlertEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getEodAlertEmail())){
			corp.setEodAlertEmail(corpDTO.getEodAlertEmail());
		}
		if(corpDTO.getInterviewIntegrationEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInterviewIntegrationEmail())){
			corp.setInterviewIntegrationEmail(corpDTO.getInterviewIntegrationEmail());
		}
		if(corpDTO.getSigningAuthorityFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityFirstName())){
			corp.setSigningAuthorityFirstName(corpDTO.getSigningAuthorityFirstName());
		}
		if(corpDTO.getSigningAuthorityMiddleName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityMiddleName())){
			corp.setSigningAuthorityMiddleName(corpDTO.getSigningAuthorityMiddleName());
		}
		if(corpDTO.getSigningAuthorityLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityLastName())){
			corp.setSigningAuthorityLastName(corpDTO.getSigningAuthorityLastName());
		}
		if(corpDTO.getSigningAuthorityPhone() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityPhone())){
			corp.setSigningAuthorityPhone(corpDTO.getSigningAuthorityPhone());
		}
		if(corpDTO.getSigningAuthorityDesignation() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityDesignation())){
			corp.setSigningAuthorityDesignation(corpDTO.getSigningAuthorityDesignation());
		}
		if(corpDTO.getSigningAuthorityEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getSigningAuthorityEmail())){
			corp.setSigningAuthorityEmail(corpDTO.getSigningAuthorityEmail());
		}
		if(corpDTO.getOwnerFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getOwnerFirstName())){
			corp.setOwnerFirstName(corpDTO.getOwnerFirstName());
		}
		if(corpDTO.getOwnerMiddleName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getOwnerMiddleName())){
			corp.setOwnerMiddleName(corpDTO.getOwnerMiddleName());
		}
		if(corpDTO.getOwnerLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getOwnerLastName())){
			corp.setOwnerLastName(corpDTO.getOwnerLastName());
		}
		if(corpDTO.getOwnerPhone() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getOwnerPhone())){
			corp.setOwnerPhone(corpDTO.getOwnerPhone());
		}
		if(corpDTO.getOwnerAlternatePhone() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getOwnerAlternatePhone())){
			corp.setOwnerAlternatePhone(corpDTO.getOwnerAlternatePhone());
		}
		if(corpDTO.getInvoiceContactFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInvoiceContactFirstName())){
			corp.setInvoiceContactFirstName(corpDTO.getInvoiceContactFirstName());
		}
		if(corpDTO.getInvoiceContactMiddleName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInvoiceContactMiddleName())){
			corp.setInvoiceContactMiddleName(corpDTO.getInvoiceContactMiddleName());
		}
		if(corpDTO.getInvoiceContactLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInvoiceContactLastName())){
			corp.setInvoiceContactLastName(corpDTO.getInvoiceContactLastName());
		}
		if(corpDTO.getInvoiceContactPhone() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInvoiceContactPhone())){
			corp.setInvoiceContactPhone(corpDTO.getInvoiceContactPhone());
		}
		if(corpDTO.getInvoiceContactEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getInvoiceContactEmail())){
			corp.setInvoiceContactEmail(corpDTO.getInvoiceContactEmail());
		}
		if(corpDTO.getPortalAdminFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getPortalAdminFirstName())){
			corp.setPortalAdminFirstName(corpDTO.getPortalAdminFirstName());
		}
		if(corpDTO.getPortalAdminMiddleName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getPortalAdminMiddleName())){
			corp.setPortalAdminMiddleName(corpDTO.getPortalAdminMiddleName());
		}
		if(corpDTO.getPortalAdminLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getPortalAdminLastName())){
			corp.setPortalAdminLastName(corpDTO.getPortalAdminLastName());
		}
		if(corpDTO.getPortalAdminPhone() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getPortalAdminPhone())){
			corp.setPortalAdminPhone(corpDTO.getPortalAdminPhone());
		}
		if(corpDTO.getPortalAdminEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(corpDTO.getPortalAdminEmail())){
			corp.setPortalAdminEmail(corpDTO.getPortalAdminEmail());
		}
		corp.setUpdatedByUserId(corpDTO.getUserId());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		corp.setUpdatdDtTm(timestamp);
		return corp;
	}

	public static Date getDate(String dateInString) {
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;

	}

	public static Recruiter convertRecruiterDTOToRecruiterObj(RecruiterDTO recruiterDTO){
		Recruiter recruiter = new Recruiter();
		if(recruiterDTO.getId() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getId())){
			recruiter.setId(Long.parseLong(recruiterDTO.getId()));
		}
	
		recruiter.setActiveFlag(Constants.Y);
		recruiter.setFirstName(recruiterDTO.getFirstName());
		recruiter.setLastName(recruiterDTO.getLastName());
		recruiter.setMiddleName(recruiterDTO.getMiddleName());
		recruiter.setOfficialEmail(recruiterDTO.getOfficialEmail());
		recruiter.setPersonalEmail(recruiterDTO.getPersonalEmail());
		recruiter.setHighestQualifincation(recruiterDTO.getHighestQualifincation());
		recruiter.setRecrutingComments(recruiterDTO.getRecrutingComments());
		recruiter.setSalary(recruiterDTO.getSalary());
		recruiter.setCommission(recruiterDTO.getCommission());
		recruiter.setPrevExp(recruiterDTO.getPrevExp());
		recruiter.setPrevCompWorked(recruiterDTO.getPrevCompWorked());
		recruiter.setNativePlace(recruiterDTO.getNativePlace());
		recruiter.setAddrLine1(recruiterDTO.getAddrLine1());
		recruiter.setAddrLine2(recruiterDTO.getAddrLine2());
		recruiter.setAddrLine3(recruiterDTO.getAddrLine3());
		recruiter.setTown(recruiterDTO.getTown());
		recruiter.setDistrict(recruiterDTO.getDistrict());
		Timestamp timestamp = new Timestamp(new Date().getTime());
		recruiter.setJoinDate(getDate(recruiterDTO.getJoinDate()));
		recruiter.setBirthDate(getDate(recruiterDTO.getBirthDate()));
		recruiter.setPassDate(getDate(recruiterDTO.getPassDate()));
		recruiter.setState(recruiterDTO.getState());
		recruiter.setZipcode(recruiterDTO.getZipcode());
		recruiter.setPermAddrLine1(recruiterDTO.getPermAddrLine1());
		recruiter.setPermAddrLine2(recruiterDTO.getPermAddrLine2());
		recruiter.setPermAddrLine3(recruiterDTO.getPermAddrLine3());
		recruiter.setPermtTown(recruiterDTO.getPermtTown());
		recruiter.setPermDistrict(recruiterDTO.getPermDistrict());
		recruiter.setPermState(recruiterDTO.getPermState());

		recruiter.setPermZipcode(recruiterDTO.getPermZipcode());
		recruiter.setCreatedDtTm(timestamp);
		recruiter.setUpdatdDtTm(timestamp);

		recruiter.setCreatedByUserId(recruiterDTO.getUserId());
		recruiter.setUpdatedByUserId(recruiterDTO.getUserId());
		recruiter.setRecruiterName(recruiterDTO.getRecruiterName());
		return recruiter;
	}

	public static RecruiterDTO convertRecruiterObjToRecruiterDTO(Recruiter recruiter){
		
		RecruiterDTO recruiterDTO = null;
		if(recruiter != null){
			recruiterDTO = new RecruiterDTO();
			recruiterDTO.setId(Long.toString(recruiter.getId()));
			recruiterDTO.setActiveFlag(recruiter.getActiveFlag());
			recruiterDTO.setFirstName(recruiter.getFirstName());
			recruiterDTO.setLastName(recruiter.getLastName());
			recruiterDTO.setMiddleName(recruiter.getMiddleName());
			recruiterDTO.setOfficialEmail(recruiter.getOfficialEmail());
			recruiterDTO.setPersonalEmail(recruiter.getPersonalEmail());
			recruiterDTO.setHighestQualifincation(recruiter.getHighestQualifincation());
			recruiterDTO.setRecrutingComments(recruiter.getRecrutingComments());
			recruiterDTO.setSalary(recruiter.getSalary());
			recruiterDTO.setCommission(recruiter.getCommission());
			recruiterDTO.setPrevExp(recruiter.getPrevExp());
			recruiterDTO.setPrevCompWorked(recruiter.getPrevCompWorked());
			recruiterDTO.setNativePlace(recruiter.getNativePlace());
			recruiterDTO.setAddrLine1(recruiter.getAddrLine1());
			recruiterDTO.setAddrLine2(recruiter.getAddrLine2());
			recruiterDTO.setAddrLine3(recruiter.getAddrLine3());
			recruiterDTO.setTown(recruiter.getTown());
			recruiterDTO.setDistrict(recruiter.getDistrict());
			recruiterDTO.setJoinDate(recruiter.getJoinDate().toString());
			recruiterDTO.setBirthDate(recruiter.getBirthDate().toString());
			recruiterDTO.setPassDate(recruiter.getPassDate().toString());
			recruiterDTO.setState(recruiter.getState());
			recruiterDTO.setZipcode(recruiter.getZipcode());
			recruiterDTO.setPermAddrLine1(recruiter.getPermAddrLine1());
			recruiterDTO.setPermAddrLine2(recruiter.getPermAddrLine2());
			recruiterDTO.setPermAddrLine3(recruiter.getPermAddrLine3());
			recruiterDTO.setPermtTown(recruiter.getPermtTown());
			recruiterDTO.setPermDistrict(recruiter.getPermDistrict());
			recruiterDTO.setPermState(recruiter.getPermState());
	
			recruiterDTO.setPermZipcode(recruiter.getPermZipcode());
			recruiterDTO.setRecruiterName(recruiter.getRecruiterName());
		}
		return recruiterDTO;
	}
	
	public static Recruiter convertRecruiterDTOToRecruiterObj(RecruiterDTO recruiterDTO,Recruiter recruiter){
		if(recruiterDTO.getActiveFlag() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getActiveFlag())){
			recruiter.setActiveFlag(recruiterDTO.getActiveFlag());
		}
	
		if(recruiterDTO.getFirstName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getFirstName())){
			recruiter.setFirstName(recruiterDTO.getFirstName());
		}
		
		if(recruiterDTO.getLastName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getLastName())){
			recruiter.setLastName(recruiterDTO.getLastName());
		}
		
		if(recruiterDTO.getMiddleName() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getMiddleName())){
			recruiter.setMiddleName(recruiterDTO.getMiddleName());
		}
		if(recruiterDTO.getOfficialEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getOfficialEmail())){
			recruiter.setOfficialEmail(recruiterDTO.getOfficialEmail());
		}
		if(recruiterDTO.getPersonalEmail() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPersonalEmail())){
			recruiter.setPersonalEmail(recruiterDTO.getPersonalEmail());
		}
		if(recruiterDTO.getHighestQualifincation() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getHighestQualifincation())){
			recruiter.setHighestQualifincation(recruiterDTO.getHighestQualifincation());
		}
		if(recruiterDTO.getRecrutingComments() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getRecrutingComments())){
			recruiter.setRecrutingComments(recruiterDTO.getRecrutingComments());
		}
		if(recruiterDTO.getSalary() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getSalary())){
			recruiter.setSalary(recruiterDTO.getSalary());
		}
		if(recruiterDTO.getCommission() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getCommission())){
			recruiter.setCommission(recruiterDTO.getCommission());
		}
		if(recruiterDTO.getPrevExp() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPrevExp())){
			recruiter.setPrevExp(recruiterDTO.getPrevExp());
		}
		if(recruiterDTO.getPrevCompWorked() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPrevCompWorked())){
			recruiter.setPrevCompWorked(recruiterDTO.getPrevCompWorked());
		}
		if(recruiterDTO.getNativePlace() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getNativePlace())){
			recruiter.setNativePlace(recruiterDTO.getNativePlace());
		}
		if(recruiterDTO.getAddrLine1() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getAddrLine1())){
			recruiter.setAddrLine1(recruiterDTO.getAddrLine1());
		}
		if(recruiterDTO.getAddrLine2() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getAddrLine2())){
			recruiter.setAddrLine2(recruiterDTO.getAddrLine2());
		}
		if(recruiterDTO.getAddrLine3() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getAddrLine3())){
			recruiter.setAddrLine3(recruiterDTO.getAddrLine3());
		}
		if(recruiterDTO.getTown() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getTown())){
			recruiter.setTown(recruiterDTO.getTown());
		}
		if(recruiterDTO.getDistrict() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getDistrict())){
			recruiter.setDistrict(recruiterDTO.getDistrict());
		}
		if(recruiterDTO.getJoinDate() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getJoinDate())){
			recruiter.setJoinDate(getDate(recruiterDTO.getJoinDate()));
		}
		if(recruiterDTO.getBirthDate() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getJoinDate())){
			recruiter.setBirthDate(getDate(recruiterDTO.getBirthDate()));
		}
		if(recruiterDTO.getPassDate() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPassDate())){
			recruiter.setPassDate(getDate(recruiterDTO.getPassDate()));
		}
		if(recruiterDTO.getState() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getState())){
			recruiter.setState(recruiterDTO.getState());
		}
		if(recruiterDTO.getZipcode() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getZipcode())){
			recruiter.setZipcode(recruiterDTO.getZipcode());
		}
		if(recruiterDTO.getPermAddrLine1() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermAddrLine1())){
			recruiter.setPermAddrLine1(recruiterDTO.getPermAddrLine1());
		}
		if(recruiterDTO.getPermAddrLine2() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermAddrLine2())){
			recruiter.setPermAddrLine2(recruiterDTO.getPermAddrLine2());
		}
		if(recruiterDTO.getPermAddrLine3() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermAddrLine3())){
			recruiter.setPermAddrLine3(recruiterDTO.getPermAddrLine3());
		}
		if(recruiterDTO.getPermtTown() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermtTown())){
			recruiter.setPermtTown(recruiterDTO.getPermtTown());
		}
		if(recruiterDTO.getPermDistrict() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermDistrict())){
			recruiter.setPermDistrict(recruiterDTO.getPermDistrict());
		}
		if(recruiterDTO.getPermState() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermState())){
			recruiter.setPermState(recruiterDTO.getPermState());
		}
		if(recruiterDTO.getPermZipcode() != null && !Constants.NULL_SPACE.equalsIgnoreCase(recruiterDTO.getPermZipcode())){
			recruiter.setPermZipcode(recruiterDTO.getPermZipcode());
		}
		
		Timestamp timestamp = new Timestamp(new Date().getTime());
		recruiter.setUpdatdDtTm(timestamp);

		recruiter.setUpdatedByUserId(recruiterDTO.getUserId());
		return recruiter;
	}

}
