package repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	 @Query("from User")
     List<User> getAllActiveAdminUsers();
    
	 User findUserDistinctByUserNameAndPassword(String userName, String password);
   
	 User findUserDistinctById(Long id);
	 User findUserDistinctByUserName(String userName);
}