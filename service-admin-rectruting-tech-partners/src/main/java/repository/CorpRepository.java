package repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Corp;

@Repository
public interface CorpRepository extends CrudRepository<Corp, Long> {

	Corp findCorpDistinctByFtinNumber(Long ftinNumber);
	Corp findCorpDistinctById(Long id);
	
	@Query("from Corp")
	List<Corp> getCorps();

}