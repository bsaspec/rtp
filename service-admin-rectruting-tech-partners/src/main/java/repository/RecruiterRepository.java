package repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Recruiter;

@Repository
public interface RecruiterRepository extends CrudRepository<Recruiter, Long> {
	 
	Recruiter findRecruiterDistinctById(Long id);
	Recruiter findRecruiterDistinctByRecruiterName(String recruiterName);
	
	@Query("from Recruiter")
	List<Recruiter> getRecruiters();	

}