package repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.UserRole;

@Repository
public interface UserRolesRepository extends CrudRepository<UserRole, Long> {
	
	
}