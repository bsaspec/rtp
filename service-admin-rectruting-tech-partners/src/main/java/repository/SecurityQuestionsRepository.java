package repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import domain.SecurityQuestionMaster;

public interface SecurityQuestionsRepository extends CrudRepository<SecurityQuestionMaster, Long>{
	
	
	
	
	@Query("from SecurityQuestionMaster")
	List<SecurityQuestionMaster> getSecurityQuestionMaster();
	
	
	
}
