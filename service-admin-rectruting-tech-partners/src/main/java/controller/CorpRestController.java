package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dto.CorpDTO;
import dto.StatusCodesDTO;
import service.CorpService;
import service.EmailService;
import service.UserService;

@RestController
public class CorpRestController {

	@Autowired
	CorpService corpService;  
	@Autowired
	UserService userService;

	@Autowired
	private EmailService emailService;
	
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/corp/create", method = RequestMethod.POST )
	public Object createCorp(@RequestBody CorpDTO corpDTO) {

		CorpDTO newCorp= null;
		try {
			if(corpDTO.getFtinNumber() != null && !"".equalsIgnoreCase(corpDTO.getFtinNumber())){
				if(corpService.findFtinNumber(Long.parseLong(corpDTO.getFtinNumber())) == null){
					//validateCorp(corpDTO);
					newCorp = corpService.saveCorp(corpDTO);
					emailService.sendMail(newCorp.getPortalAdminEmail(),newCorp.getUserId(),newCorp.getPortalAdminFirstName());
				}else{
					StatusCodesDTO status = new StatusCodesDTO();
					status.setStatusCode(406);
					status.setStatusMessage("Not acceptable:Duplicate ftinNumber");
					return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
				}
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("ftinNumber canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while creating a company.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while creating a company.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<CorpDTO>(newCorp, HttpStatus.OK);
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/corp/findAllCorps", method = RequestMethod.GET )
	public Object findAllCorps() {

		List<CorpDTO> corps = null;
		try {
			corps = corpService.findAllCorps();
			if(corps != null && corps.size() > 0){
				return new ResponseEntity<List<CorpDTO>>(corps, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No companies are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a company's list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a company's list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/corp/searchCorpDetails", method = RequestMethod.POST )
	public Object searchCorpDetails(@RequestBody CorpDTO corpDTO) {
		List<CorpDTO> corps = null;
		try {
			corps = corpService.searchCorpDetails(corpDTO.getCompanyName(),corpDTO.getFtinNumber());
			if(corps != null && corps.size() > 0){
				return new ResponseEntity<List<CorpDTO>>(corps, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No companies are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a company's list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a company's list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/corp/deleteCorp", method = RequestMethod.POST )
	public Object deleteCorp(@RequestBody CorpDTO corpDTO) {
		CorpDTO newCorp= null;
		try {
			if(corpDTO.getId() != null && !"".equalsIgnoreCase(corpDTO.getId())){
				newCorp = corpService.deleteCorp(Long.parseLong(corpDTO.getId()));
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("Comapy Id canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while deletinng  a company.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while deletinng a company.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<CorpDTO>(newCorp, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/corp/updateCorp", method = RequestMethod.POST )
	public Object updateCorp(@RequestBody CorpDTO corpDTO) {
		CorpDTO newCorp= null;
		try {
			newCorp = corpService.updateCorp(corpDTO);
		} catch (Exception e) {
			System.out.println("There is a issue while updating  a company.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while updating a company.");
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<CorpDTO>(newCorp, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/corp/findAllActiveCorps", method = RequestMethod.GET )
	public Object findAllActiveCorps() {

		List<CorpDTO> corps = null;
		try {
			corps = corpService.findAllActiveCompanies();
			if(corps != null && corps.size() > 0){
				return new ResponseEntity<List<CorpDTO>>(corps, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No Active companies are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a Active company's list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a Active company's list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/corp/getCorp", method = RequestMethod.POST )
	public Object getCorp(@RequestBody CorpDTO corpDTO) {
		CorpDTO newCorp= null;
		try {
			if(corpDTO.getId() != null && !"".equalsIgnoreCase(corpDTO.getId())){
				newCorp = corpService.getCorp(Long.parseLong(corpDTO.getId()));
				if(newCorp == null){
					StatusCodesDTO status = new StatusCodesDTO();
					status.setStatusCode(406);
					status.setStatusMessage("There is no company with this id "+corpDTO.getId());
					return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
				}
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("Comapy Id canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting  a company.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a company.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<CorpDTO>(newCorp, HttpStatus.OK);
	}
	
}
