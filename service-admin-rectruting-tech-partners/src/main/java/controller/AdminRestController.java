package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dto.StatusCodesDTO;
import dto.UserDTO;
import service.CorpService;
import service.UserService;

@RestController
public class AdminRestController {

	@Autowired
	CorpService corpService;

	//@Autowired
	//private EmailService emailService;
	
	@Autowired
	private UserService userService;

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/admin/create", method = RequestMethod.POST )
	public Object createAdmin(@RequestBody UserDTO userDTO) {
		UserDTO newAdmin= null;
		try {
			if(userDTO.getCorpId() != null && !"".equalsIgnoreCase(userDTO.getCorpId())){
				if(userDTO.getUserName() != null && !"".equalsIgnoreCase(userDTO.getUserName())){
					if(userService.findUserDistinctByUserName(userDTO.getUserName()) == null){
						//validateAdmin(corpDTO);
						newAdmin = userService.saveUser(userDTO);
						//emailService.sendMail(userDTO.getEmail());
					}else{
						StatusCodesDTO status = new StatusCodesDTO();
						status.setStatusCode(406);
						status.setStatusMessage("Not acceptable:Duplicate User Name");
						return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
					}
				}else{
					StatusCodesDTO status = new StatusCodesDTO();
					status.setStatusCode(406);
					status.setStatusMessage("Username  canot be null");
					return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
				}
			}
			else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("CorpId canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while creating a Admin.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while creating a Admin.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<UserDTO>(newAdmin, HttpStatus.OK);
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/admin/findAllUsers", method = RequestMethod.GET )
	public Object findAllAdmins() {

		List<UserDTO> admins = null;
		try {
			admins = userService.findAllUsers();
			if(admins != null && admins.size() > 0){
				return new ResponseEntity<List<UserDTO>>(admins, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No Users are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a users list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a users list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/admin/searchUserDetails", method = RequestMethod.POST )
	public Object searchAdminDetails(@RequestBody UserDTO userDTO) {
		List<UserDTO> admins = null;
		try {
			admins = userService.searchUserDetails(userDTO);
			if(admins != null && admins.size() > 0){
				return new ResponseEntity<List<UserDTO>>(admins, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No users are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a users list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a users list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/admin/deleteUser", method = RequestMethod.POST )
	public Object deleteAdmin(@RequestBody UserDTO userDTO) {
		UserDTO newAdmin= null;
		try {
			if(userDTO.getId() != null && !"".equalsIgnoreCase(userDTO.getId())){
				newAdmin = userService.deleteUser(Long.parseLong(userDTO.getId()));
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("User Id canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while deletinng  a User.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while deletinng a User.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<UserDTO>(newAdmin, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/admin/updateUser", method = RequestMethod.POST )
	public Object updateAdmin(@RequestBody UserDTO userDTO) {
		UserDTO newAdmin= null;
		try {
			newAdmin = userService.updateUser(userDTO);
		} catch (Exception e) {
			System.out.println("There is a issue while deletinng  a User.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while deletinng a User.");
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<UserDTO>(newAdmin, HttpStatus.OK);
	}
	 
}
