package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dto.RecruiterDTO;
import dto.StatusCodesDTO;
import service.CorpService;
import service.EmailService;
import service.RecruiterService;

@RestController
public class RecruiterRestController {

	@Autowired
	RecruiterService recruiterService; 

	@Autowired
	CorpService corpService;

	@Autowired
	private EmailService emailService;

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/recruiter/create", method = RequestMethod.POST )
	public Object createRecruiter(@RequestBody RecruiterDTO recruiterDTO) {
		RecruiterDTO newRecruiter= null;
		try {
			if(recruiterDTO.getCorpId() != null && !"".equalsIgnoreCase(recruiterDTO.getCorpId())){
				if(recruiterDTO.getRecruiterName() != null && !"".equalsIgnoreCase(recruiterDTO.getRecruiterName())){
					if(recruiterService.findRecruiterDistinctByRecruiterName(recruiterDTO.getRecruiterName()) == null){
						//validateRecruiter(corpDTO);
						newRecruiter = recruiterService.saveRecruiter(recruiterDTO);
						emailService.sendMail(newRecruiter.getPersonalEmail(),newRecruiter.getUserId(),newRecruiter.getRecruiterName());
					}else{
						StatusCodesDTO status = new StatusCodesDTO();
						status.setStatusCode(406);
						status.setStatusMessage("Not acceptable:Duplicate Recruiter Name");
						return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
					}
				}else{
					StatusCodesDTO status = new StatusCodesDTO();
					status.setStatusCode(406);
					status.setStatusMessage("Recruiter Name canot be null");
					return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
				}
			}
			else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("CorpId canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while creating a Recruiter.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while creating a Recruiter.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<RecruiterDTO>(newRecruiter, HttpStatus.OK);
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/recruiter/findAllRecruiters", method = RequestMethod.GET )
	public Object findAllRecruiters() {

		List<RecruiterDTO> recruiters = null;
		try {
			recruiters = recruiterService.findAllRecruiters();
			if(recruiters != null && recruiters.size() > 0){
				return new ResponseEntity<List<RecruiterDTO>>(recruiters, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No recruiters are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a recruiter's list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a recruiter's list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/recruiter/searchRecruiterDetails", method = RequestMethod.POST )
	public Object searchRecruiterDetails(@RequestBody RecruiterDTO recruiterDTO) {
		List<RecruiterDTO> recruiters = null;
		try {
			recruiters = recruiterService.searchRecruiterDetails(recruiterDTO.getRecruiterName(),recruiterDTO.getFtinNumber());
			if(recruiters != null && recruiters.size() > 0){
				return new ResponseEntity<List<RecruiterDTO>>(recruiters, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No recruiters are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a recruiters list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a recruiters list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/recruiter/deleteRecruiter", method = RequestMethod.POST )
	public Object deleteRecruiter(@RequestBody RecruiterDTO recruiterDTO) {
		RecruiterDTO newrecruiter= null;
		try {
			if(recruiterDTO.getId() != null && !"".equalsIgnoreCase(recruiterDTO.getId())){
				newrecruiter = recruiterService.deleteRecruiter(Long.parseLong(recruiterDTO.getId()));
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("recruiter Id canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while deletinng  a recruiter.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while deletinng a recruiter.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<RecruiterDTO>(newrecruiter, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/recruiter/updateRecruiter", method = RequestMethod.POST )
	public Object updateRecruiter(@RequestBody RecruiterDTO recruiterDTO) {
		RecruiterDTO newRecruiter= null;
		try {
			newRecruiter = recruiterService.updateRecruiter(recruiterDTO);
		} catch (Exception e) {
			System.out.println("There is a issue while updating  a Recruiter.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while updating a Recruiter.");
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<RecruiterDTO>(newRecruiter, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/recruiter/findAllActiveRecruiters", method = RequestMethod.GET )
	public Object findAllActiveRecruiters() {

		List<RecruiterDTO> recruiters = null;
		try {
			recruiters = recruiterService.findAllActiveRecruiters();
			if(recruiters != null && recruiters.size() > 0){
				return new ResponseEntity<List<RecruiterDTO>>(recruiters, HttpStatus.OK);
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("No Active Recruiters are avilable");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting a Active Recruiters list.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a Active Recruiters list.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/recruiter/getRecruiter", method = RequestMethod.POST )
	public Object getRecruiter(@RequestBody RecruiterDTO recuriterDTO) {
		RecruiterDTO newRecruiter= null;
		try {
			if(recuriterDTO.getId() != null && !"".equalsIgnoreCase(recuriterDTO.getId())){
				newRecruiter = recruiterService.getRecruiter(Long.parseLong(recuriterDTO.getId()));
				if(newRecruiter == null){
					StatusCodesDTO status = new StatusCodesDTO();
					status.setStatusCode(406);
					status.setStatusMessage("There is no recruiter with this id "+recuriterDTO.getId());
					return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
				}
			}else{
				StatusCodesDTO status = new StatusCodesDTO();
				status.setStatusCode(406);
				status.setStatusMessage("recruiter Id canot be null");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			System.out.println("There is a issue while getting  a recruiter.");
			StatusCodesDTO status = new StatusCodesDTO();
			status.setStatusCode(500);
			status.setStatusMessage("There is a issue while getting a recruiter.");
			e.printStackTrace();
			return new ResponseEntity(status,HttpStatus.METHOD_FAILURE);
		}
		return new ResponseEntity<RecruiterDTO>(newRecruiter, HttpStatus.OK);
	}
	 
}
