package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import dto.SecurityQuestionMasterDto;
import dto.SecurityQuestionWrapper;
import dto.StatusCodesDTO;
import dto.UserDTO;
import dto.UserSecurityQuestionDto;
import service.LoginService;

@SuppressWarnings({ "unchecked", "rawtypes" })
@RestController
public class LoginRestController {

	@Autowired
	LoginService loginService;  //Service which will do all data retrieval/manipulation work

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Object validateCredentials(@RequestParam("userName") String userName, 
			@RequestParam("password") String password) {
		System.out.println("userName " + userName);
		System.out.println("password " + password);
		UserDTO user = null;
		StatusCodesDTO status = new StatusCodesDTO();
		try {
			user = loginService.loginUser(userName, password);
			
			if (user==null) {
				System.out.println("User with id " + userName + " not found");
				status.setStatusCode(406);
				status.setStatusMessage("Invalid user Name and Password");
				return new ResponseEntity(status,HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			status.setStatusCode(500);
			status.setStatusMessage("Internal Server Error");
			return new ResponseEntity(status,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UserDTO>(user, HttpStatus.OK);
	}
	
	/*
	 * @ Author Rajiv
	 * 
	 * */
	@RequestMapping(value = "/getSecurityQuestions", method = RequestMethod.GET)
	public Object userSecurity() {
		StatusCodesDTO status = new StatusCodesDTO();
		try{
			return	new ResponseEntity<List<SecurityQuestionMasterDto>>(loginService.getSecurityQuestions(), HttpStatus.OK);
		} catch (Exception runtimeException){
			status.setStatusCode(500);
			status.setStatusMessage("Internal Server Error"+runtimeException);
			return new ResponseEntity(status,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	/*
	 * @ Author Rajiv
	 * 
	 * */
	@RequestMapping(value = "/getUserAnswer", method = RequestMethod.POST)
	public Object userAnswer(Long userId) {
		StatusCodesDTO status = new StatusCodesDTO();
		try{
			return	new ResponseEntity<List<UserSecurityQuestionDto>>(loginService.getCheckUser(userId), HttpStatus.OK);
		} catch (Exception runtimeException){
			runtimeException.printStackTrace();
			status.setStatusCode(500);
			status.setStatusMessage("Internal Server Error"+"  "+runtimeException);
			return new ResponseEntity(status,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	/*
	 * @ Author Rajiv
	 * 
	 * */
	@RequestMapping(value = "/saveSecurityAnswers", method = RequestMethod.POST)
	public Object saveSecurityAnswers(String answers) {
		return loginService.saveAnswers(new Gson().fromJson(answers, SecurityQuestionWrapper.class));
	}
	

}
