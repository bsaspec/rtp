package com.rtp.ui.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Spring Source Tool Suite.
 * 
 * @author surendra
 */
@Configuration
@ComponentScan(basePackages = {"com.rtp.ui.config","com.rtp.ui.controller"})
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
