package com.rtp.ui.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rtp.ui.dto.SecurityQuestionMasterDto;
import com.rtp.ui.dto.SecurityQuestionWrapper;
import com.rtp.ui.dto.UserDTO;
import com.rtp.ui.util.BaseUtil;
/**
 * Controller that demonstrates tiles mapping, reguest parameters and path variables.
 * 
 * @author surendra
 */
@Controller
@PropertySource("classpath:application.properties")
public class LoginController {
	private Log log = LogFactory.getLog(this.getClass());
	
	
	@Autowired
	private Environment env;
	
	
	private final String USER_AGENT = "Mozilla/5.0";
    @RequestMapping(value = "/login", method=RequestMethod.GET)
	public String login() {
	    return "site.login";
	}
    
    @RequestMapping(value = "/loginSubmit", method=RequestMethod.POST)
	public String home(@RequestParam("username") String username,@RequestParam String password,ModelMap modelMap,
			HttpSession httpSession) {
    	String admin_service_url=env.getProperty("admin.service.url");
    	 try {
		UserDTO userDTO=sendPost(username, password, "");
		if(userDTO!=null&&userDTO.getUserId()!=null&&userDTO.getEnabled().equalsIgnoreCase("Y")){
			
			
			httpSession.setAttribute("userDTO", userDTO);
			System.out.println(admin_service_url+":::admin.service.url****************");
			httpSession.setAttribute("adminServiceURl", admin_service_url);
			return "site.corp";
		}else if(userDTO.getEnabled().equalsIgnoreCase("N")){
			modelMap.addAttribute("username",username);
			
			String jspnResponse = BaseUtil.sendGEt(null, admin_service_url+"getSecurityQuestions");
			System.out.println(jspnResponse+"::securityyy");
			if (jspnResponse != null) {
				System.out.println("got");
				Gson gson = new Gson();
				Type type = new TypeToken<List<SecurityQuestionMasterDto>>() {}.getType();
				List<SecurityQuestionMasterDto> list = gson.fromJson(jspnResponse, type);
				System.out.println(list.size()+"-->size");
				modelMap.addAttribute("questionList",list);
				modelMap.addAttribute("username",username);
				modelMap.addAttribute("userid",userDTO.getUserId());
			}
			    return "site.newUser";
		}
		} catch (Exception e) {
			e.printStackTrace();
		} 
       modelMap.addAttribute("error", "Invalid username/password");    	 
	    return "site.login";
	}
	
    
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    @ResponseBody
    public String getUserInfo (HttpSession session) {
    	UserDTO nUser = (UserDTO)session.getAttribute("userDTO");
    	String adminServiceURl=(String)session.getAttribute("adminServiceURl");
    	Gson gson=new Gson();
    	String userData=gson.toJson(nUser);
        return userData+"&&"+adminServiceURl;
    }
    
    
    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
	public String newUsers(@RequestParam("userid") String userid,@RequestParam("username") String username,@RequestParam("psw") String psw,ModelMap modelMap) {
		System.out.println(psw + "::::::::::::::psw");
		String admin_service_url=env.getProperty("admin.service.url");
		try {
			String jspnResponse = BaseUtil.sendGEt(null, admin_service_url+"getSecurityQuestions");
			System.out.println(jspnResponse+"::json");
			if (jspnResponse != null) {
				System.out.println("got");
				Gson gson = new Gson();
				Type type = new TypeToken<List<SecurityQuestionMasterDto>>() {}.getType();
				List<SecurityQuestionMasterDto> list = gson.fromJson(jspnResponse, type);
				System.out.println(list.size()+"-->size");
				modelMap.addAttribute("questionList",list);
				modelMap.addAttribute("username",username);
				modelMap.addAttribute("userid",userid);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "site.newUser";
	}
 
    
    @RequestMapping(value = "/quetionsSubmit", method=RequestMethod.POST)
   	public String quetionsSubmit(@RequestParam("userid") String userid,@RequestParam("password") String password,@RequestParam String answer1,
   			@RequestParam String answer2,@RequestParam String answer3,
   			@RequestParam String quetionId1,
   			@RequestParam String quetionId2,@RequestParam String quetionId3,
   			ModelMap modelMap,HttpSession httpSession) {
    	String admin_service_url=env.getProperty("admin.service.url");
    	SecurityQuestionWrapper securityQuestionWrapper=new SecurityQuestionWrapper();
    	
       	List<SecurityQuestionMasterDto> securityQuestionMasterDtos=new ArrayList<SecurityQuestionMasterDto>();
       	 try {
       		SecurityQuestionMasterDto securityQuestionMasterDto=new SecurityQuestionMasterDto();
       		securityQuestionMasterDto.setQuestionId(Long.parseLong(quetionId1));
       		securityQuestionMasterDto.setAnswer(answer1);
   		
       		SecurityQuestionMasterDto securityQuestionMasterDto2=new SecurityQuestionMasterDto();
       		securityQuestionMasterDto2.setQuestionId(Long.parseLong(quetionId2));
       		securityQuestionMasterDto2.setAnswer(answer2);
       		
       		SecurityQuestionMasterDto securityQuestionMasterDto3=new SecurityQuestionMasterDto();
       		securityQuestionMasterDto3.setQuestionId(Long.parseLong(quetionId3));
       		securityQuestionMasterDto3.setAnswer(answer3);
       		
       		securityQuestionMasterDtos.add(securityQuestionMasterDto);
       		securityQuestionMasterDtos.add(securityQuestionMasterDto2);
       		securityQuestionMasterDtos.add(securityQuestionMasterDto3);
       		
       		
       		securityQuestionWrapper.setUserCheckins(securityQuestionMasterDtos);
       		
       		UserDTO userDTO=(UserDTO) httpSession.getAttribute("userDTO");
       		
       		securityQuestionWrapper.setUserId(Long.parseLong(userid));
       		securityQuestionWrapper.setPassword(password);
       		
       		Gson gson=new Gson();
       		String dataJson=gson.toJson(securityQuestionWrapper);
       		System.out.println(securityQuestionWrapper);
   		    BaseUtil.sendPost(dataJson, admin_service_url+"saveSecurityAnswers");
   			return "site.login";
   		
   		} catch (Exception e) {
   			e.printStackTrace();
   		} 
          modelMap.addAttribute("error", "Invalid username/password");    	 
   	    return "site.newUser";
   	}
    
    private UserDTO sendPost(String username,String password,String url) throws Exception {
    	UserDTO userDTO=null;
    	String admin_service_url=env.getProperty("admin.service.url");
		 url = admin_service_url+"login";
		URL obj = new URL(url);
		 java.net.HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "userName="+username+"&password="+password;

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
         if(response!=null){
        	 Gson gson=new Gson();
        	 userDTO= gson.fromJson(response.toString(), UserDTO.class);
         }
		return userDTO;
	}

}
