package com.rtp.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CorpController {
	@RequestMapping(value = "/createCompany", method = RequestMethod.GET)
	public String login() {
		return "site.homepage";
	}

	@RequestMapping(value = "/createAdmin", method = RequestMethod.GET)
	public String createAdmin() {
		return "site.admin";
	}

	@RequestMapping(value = "/companies", method = RequestMethod.GET)
	public String companies() {
		return "site.companies";
	}

	@RequestMapping(value = "/adminUsers", method = RequestMethod.GET)
	public String adminUsers() {
		return "site.adminUsers";
	}

	@RequestMapping(value = "/createRecruiters", method = RequestMethod.GET)
	public String createRecruiters() {
		return "site.createRecruiters";
	}

	@RequestMapping(value = "/recruiters", method = RequestMethod.GET)
	public String recruiters() {

		return "site.recruiters";
	}
	
	
	
	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public String assignRoles() {

		return "site.assignRoles";
	}


	

}
