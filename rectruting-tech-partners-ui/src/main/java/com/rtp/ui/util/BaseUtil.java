package com.rtp.ui.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.rtp.ui.dto.UserDTO;

public class BaseUtil {
	
	private static String USER_AGENT = "Mozilla/5.0";
	 public static String sendGEt(String urlParameters,String restURl) throws Exception {

			
			System.out.println(restURl+":::complete url");
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(restURl);
			request.addHeader("User-Agent", USER_AGENT);
			HttpResponse response = client.execute(request);
			if(response!=null){
			System.out.println("Response Code : " + 
	                       response.getStatusLine().getStatusCode());
	         if(response.getStatusLine().getStatusCode()==204){
	        	 return null;
	         }
			BufferedReader rd = new BufferedReader(
	                       new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			System.out.println(result.toString());
			return result.toString();
			}else{
				return null;
			}
	 }

	 
	 public static com.rtp.ui.dto.StatusCodesDTO  sendPost(String data,String url) throws Exception {
	    	UserDTO userDTO=null;
	    	//http://localhost:9062/
			
			URL obj = new URL(url);
			 java.net.HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			
			String urlParameters = "answers="+data;
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + data);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response);
			return null;
		}
}
