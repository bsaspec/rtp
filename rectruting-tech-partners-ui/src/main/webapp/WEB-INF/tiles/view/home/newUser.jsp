<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<body class="fix-sidebar content-wrapper">
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Enter Details</h4>
        </div>
        
        <!-- /.col-lg-12 -->
      </div>
     
      <div class="row">
       <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Confirm Details</h3>
            <p class="text-muted m-b-30 font-13"></p>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <form action="./quetionsSubmit" method="post">
                
                  <div class="form-group">
                    <label for="exampleInputEmail1">UserName</label>
                    <input type="text" class="form-control" id="username"  value="${username}" disabled="disabled">
                          <input type="text" class="form-control"  name="userid" value="${userid}" style="display: none;">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">New Password *</label>
                    <input type="password" class="form-control" id="newPassword" placeholder="Enter New Password" required="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password *</label>
                    <input type="password" class="form-control" id="confirmPassword" placeholder="Re-Enter Password" required="" name="password">
                  </div>
                  
                  <p class="text-muted m-b-30 font-13">Security Questions</p>
                  
                
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Question 1 *</label>
                    <input type="text" class="form-control" id="question1" required="" value="${questionList[0].question}" disabled="disabled">
                     <input type="text" value="${questionList[0].questionId}" style="display: none;" name="quetionId1">
                       <label>Answer</label>
                      <input type="text" class="form-control" name="answer1" required="" value="" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Question 2 *</label>
                   <input type="text" class="form-control" id="question2" required="" value="${questionList[1].question}" disabled="disabled">
                     <input type="text" value="${questionList[1].questionId}" style="display: none;" name="quetionId2">
                       <label>Answer</label>
                      <input type="text" class="form-control" name="answer2" required="" value="">
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Question 3 *</label>
                  <input type="text" class="form-control" id="question3" required="" value="${questionList[2].question}" disabled="disabled">
                    <input type="text" value="${questionList[2].questionId}" style="display: none;" name="quetionId3">
                       <label>Answer</label>
                      <input type="text" class="form-control" name="answer3" required="" value="" >
                  </div>
                  
               <%--    <div class="form-group">
                    <label for="exampleInputPassword1">Question 4 *</label>
                    <input type="text" class="form-control" id="question4" required="" value="${questionList[3].question}" disabled="disabled">
                       <label>Answer</label>
                      <input type="text" class="form-control" name="answer4" required="" value="" disabled="disabled">
                  </div> --%>
                  
                  
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                  <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>
       
      </div>
      <!-- .right-sidebar -->
      <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
          <div class="r-panel-body">
            <ul>
              <li><b>Layout Options</b></li>
              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-warning">
                  <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                  <label for="checkbox2" > Fix Sidebar </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-success">
                  <input id="checkbox4" type="checkbox" class="open-close">
                  <label for="checkbox4" > Toggle Sidebar </label>
                </div>
              </li>
            </ul>
            <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 chatonline">
              <li><b>Chat option</b></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /.right-sidebar -->
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2016 &copy; Elite Admin brought to you by themedesigner.in </footer>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="./bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="./js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="./js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./js/custom.min.js"></script>
<script src="./js/jasny-bootstrap.js"></script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
$( "#confirmPassword" ).change(function() {
	var newPassword=$("#newPassword").val();
	var confirmPassword=$("#confirmPassword").val();
	if(newPassword !== confirmPassword){
		$("#newPassword").val("");
		$("#newPassword").focus();
		$("#confirmPassword").val("");
		alert("New password is different from confirmed password")
		return false;
	}
	});


</script>
</body>
</html>
