<!DOCTYPE html>
<html lang="en">
<head>
<title>Companies</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="./js/jquery.min.js"></script>
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/custom.css" id="theme"  rel="stylesheet">

<link href="css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="fix-sidebar">
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper"  ng-app="myApp" ng-controller="corpSearchCtl">
  
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Companies List</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!--  -->
      
      <div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Search Company</label> 
					<input type="text" id="companyName" class="form-control" required="" ng-model="companyName">
						</div>
						</div>
							<!---->
						<div class="col-md-6">
							<div class="form-group">
												<label class="control-label">Search FTIN
													</label>
													 <input type="text" id="companyAddressLine1" ng-model="companyAddressLine1" class="form-control" required="">
											</div>
										</div>
										<!---->
									</div>
      <div class="row">
        
        <div class="col-sm-12">
          <div class="white-box">
           <!--  <h3 class="box-title m-b-0">Registered Companies</h3>
            <p class="text-muted m-b-30">Export data to Copy, CSV, Excel, PDF & Print</p> -->
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>CompanyName</th>
                            <th>FTINNumber</th>
                            <th>Owner</th>
                            <th>OwnerPhone</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>CompanyName</th>
                            <th>FTINNumber</th>
                            <th>Owner</th>
                            <th>OwnerPhone</th>
                            <th>AdminEmail</th>
                            <th>City</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <tr ng-repeat="corp in corps">
                    <td class="span2" ng-click="update(corp)" >{{corp.companyName}}</td>
                    <td class="span2" >{{corp.ftinNumber}}</td>
                    <td class="span2" >{{corp.ownerFirstName}}</td>
                    <td class="span2" >{{corp.ownerPhone}}</td>
                    <td class="span2" ><input type="button" ng-click="update(corp)" value="Edit"></td>
		            <td class="span2" ><input type="button" ng-click="deleteCorp(corp)" value="Remove"></td>
                   </tr>
                       
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- .right-sidebar -->
      <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i><> </div>
          <div class="r-panel-body">
            <ul>
              <li><b>Layout Options</b></li>
              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-warning">
                  <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                  <label for="checkbox2" > Fix Sidebar </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-success">
                  <input id="checkbox4" type="checkbox" class="open-close">
                  <label for="checkbox4" > Toggle Sidebar </label>
                </div>
              </li>
            </ul>
            <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 chatonline">
              <li><b>Chat option</b></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small><></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /.right-sidebar -->
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2016 &copy; Elite Admin brought to you by themedesigner.in </footer>
  </div>
  <!-- /#page-wrapper -->
  
  

   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#updatemodal" id="updatemodalId" style="display:none"/>
   

<!-- Modal -->
<form  role="form" name="companyCreateForm" ng-submit="submitForm()">
<div id="updatemodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Company Info</h4>
      </div>
      <div class="modal-body">
     
								<div class="form-body">
								<input type="text" class="form-control"  ng-model="corp.id" required name="id" style="display: none">
								
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">CompanyName</label> 
												<input type="text" class="form-control"  ng-model="corp.companyName" required name="companyName">
												<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyName.$invalid">
                                                 Company Name mandatory.
                                           </p>
											</div>
											
										</div>
										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Company Address Line 1
													</label>
													 <input type="text" id="companyAddressLine1" ng-model="corp.addressLine1" class="form-control" name="companyAddressLine1" required>
											<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyAddressLine1.$invalid">
                                                CompanyAddressLine1 is mandatory.
                                           </p>		 
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Company Address Line 2
													</label> <input type="text" id="companyAddressLine2" ng-model="corp.addressLine2" class="form-control" name="companyAddressLine2" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyAddressLine2.$invalid">
                                               CompanyAddressLine2 is mandatory.
                                                </p>		
											</div>
										</div>
										</div>
										<!--/span-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">City</label> <input
													type="text" id="city" class="form-control"  ng-model="corp.city" name="city" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.city.$invalid">
                                               City is mandatory.
                                                </p>	
											</div>
										</div>

									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">State</label> <input
													type="text" id="state" class="form-control"  ng-model="corp.state" name="state" required>
														<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.state.$invalid">
                                                         State is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Zip Code</label> <input
													type="text" id="state" class="form-control"  ng-model="corp.zip" name="zip" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.zip.$invalid">
                                                         Zip is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->



									<!--/row-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Alert Email Address</label> <input
													type="text" id="alertEmailAddress" class="form-control"
													 ng-model="corp.eodAlertEmail" name="alertEmailAddress" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.alertEmailAddress.$invalid">
                                                         Alert Email Address is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Interview Integration
													Email</label> <input type="text" id="interviewIntegrationEmail"
													class="form-control"  ng-model="corp.interviewIntegrationEmail" name="interviewIntegrationEmail" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.interviewIntegrationEmail.$invalid">
                                                        Interview Integration is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">FTIN Number</label> <input
													type="text" id="ftinNumber" class="form-control"
													 ng-model="corp.ftinNumber" name="ftinNumber" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ftinNumber.$invalid">
                                                        FTIN Number is mandatory.
                                                       </p>	
											</div>
										</div>
                                    </div>
										<!--/span-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Signing Authority First
													Name</label> <input type="text" id="signingAuthorityFirstName"
													class="form-control"  ng-model="corp.signingAuthorityFirstName" name="signingAuthorityFirstName" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityFirstName.$invalid">
                                                        Signing Authority First is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Signing Authority
													Middle Name</label> <input type="text"
													id="signingAuthorityMiddleName" class="form-control"
													 ng-model="corp.signingAuthorityMiddleName" name="signingAuthorityMiddleName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityMiddleName.$invalid">
                                                        Signing Authority Middle Name is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Signing Authority Last
													Name</label> <input type="text" id="signingAuthorityLastName"
													class="form-control"  ng-model="corp.signingAuthorityLastName" name="signingAuthorityLastName" required>
													
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityLastName.$invalid">
                                                        Signing Authority Last Name is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">SigningAuthorityPhoneNumber</label> <input type="text"
													id="signingAuthorityPhoneNumber" class="form-control"
													 ng-model="corp.signingAuthorityPhone" name="signingAuthorityPhoneNumber" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityPhoneNumber.$invalid">
                                                        Signing Authority PhoneNumber is mandatory.
                                                       </p>	 
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">SigningAuthorityDesignation</label> <input type="text"
													id="signingAuthorityDesignation" class="form-control"
													 ng-model="corp.signingAuthorityDesignation" name="signingAuthorityDesignation" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityDesignation.$invalid">
                                                        Signing AuthorityDesignation is mandatory.
                                                       </p>	 
											</div>
										</div>
										<!--/span-->
								
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Signing Authority Email</label> <input type="text" id="signingAuthorityEmail"
													class="form-control"  ng-model="corp.signingAuthorityEmail" name="signingAuthorityEmail" required>
														<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityEmail.$invalid">
                                                          Signing Authority Email is mandatory.
                                                       </p>	 
											</div>
										</div>
                                    </div>
										<!--/span-->
								<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">OwnerFirstName</label> <input
													type="text" id="ownerFirstName" class="form-control"
													 ng-model="corp.ownerFirstName" name="ownerFirstName" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerFirstName.$invalid">
                                                        Owner First Name is mandatory.
                                                       </p>	
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">OwnerMiddleName</label> <input
													type="text" id="ownerMiddleName" class="form-control"
													 ng-model="corp.ownerMiddleName" name="ownerMiddleName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerMiddleName.$invalid">
                                                        Owner Middle Name is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">OwnerLastName</label> <input
													type="text" id="ownerLastName" class="form-control"
													 ng-model="corp.ownerLastName" name="ownerLastName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerLastName.$invalid">
                                                        Owner Last Name is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">OwnerPhoneNumber</label> <input
													type="text" id="ownerPhoneNumber" class="form-control"
													 ng-model="corp.ownerPhone" name="ownerPhoneNumber" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerPhoneNumber.$invalid">
                                                        OwnerPhoneNumberismandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">OwnerAlternatePhoneNumber</label> <input type="text" id="ownerAlternatePhoneNumber"
													class="form-control"  ng-model="corp.ownerAlternatePhone" name="ownerAlternatePhoneNumber" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerAlternatePhoneNumber.$invalid">
                                                        Owner Alternate PhoneNumber is mandatory.
                                                       </p>
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">InvoiceContactFirstName</label> <input type="text" id="invoiceContactFirstName"
													class="form-control"  ng-model="corp.invoiceContactFirstName" required name="invoiceContactFirstName">
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactFirstName.$invalid">
                                                       Invoice Contact FirstName is mandatory.
                                                       </p>		
											</div>
										</div>
                                      </div>
										<!--/span-->
									<div class="row">	
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">InvoiceContactMiddleName</label> <input type="text" id="invoiceContactMiddleName"
													class="form-control"  ng-model="corp.invoiceContactMiddleName" name="invoiceContactMiddleName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactMiddleName.$invalid">
                                                      Invoice Contact MiddleName  is mandatory.
                                                       </p>				
											</div>
										</div>
										<!--/span-->
									
								
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">InvoiceContactLastName</label> <input type="text" id="invoiceContactLastName"
													class="form-control"  ng-model="corp.invoiceContactLastName" required name="invoiceContactLastName">
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactLastName.$invalid">
                                                     Invoice Contact LastName is mandatory.
                                                       </p>					
													
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">InvoiceContactPhoneNumber</label> <input type="text" id="invoiceContactPhoneNumber"
													class="form-control"  ng-model="corp.invoiceContactPhone" name="invoiceContactPhoneNumber" required>
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactPhoneNumber.$invalid">
                                                     Invoice Contact PhoneNumber is mandatory.
                                                       </p>		
													
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">InvoiceContactEmail</label>
												<input type="text" id="invoiceContactEmail"
													class="form-control"  ng-model="corp.invoiceContactEmail" required name="invoiceContactEmail">
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactEmail.$invalid">
                                                     Invoice Contact Email is mandatory.
                                                       </p>				
													
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">PortalAdminFirstName
													</label> <input type="text" id="portalAdminFirstName"
													class="form-control"  ng-model="corp.portalAdminFirstName" name="portalAdminFirstName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminFirstName.$invalid">
                                                     Portal Admin First Name is mandatory.
                                                       </p>				
													
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">PortalAdminMiddleName</label> <input type="text" id="portalAdminMiddleName"
													class="form-control"  ng-model="corp.portalAdminMiddleName" name="portalAdminMiddleName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminMiddleName.$invalid">
                                                    Portal Admin Middle is mandatory.
                                                       </p>				
											</div>
										</div>
									</div>	
                                   	<div class="row">
										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">PortalAdminLast Name
													</label> <input type="text" id="portalAdminLastName"
													class="form-control"  ng-model="corp.portalAdminLastName" name="portalAdminLastName" required>
													
								 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminLastName.$invalid">
                                                   Portal Admin Last Name is mandatory.
                                                       </p>							
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">PortalAdminPhoneNumber</label> <input type="text" id="portalAdminPhoneNumber"
													class="form-control"  ng-model="corp.portalAdminPhone" name="portalAdminPhoneNumber" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminPhoneNumber.$invalid">
                                                  Portal Admin PhoneNumber is mandatory.
                                                       </p>		
											</div>
										</div>

										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">PortalAdminEmail</label> <input
													type="text" id="portalAdminEmail" class="form-control"
													 ng-model="corp.portalAdminEmail" name="portalAdminEmail" required>
										 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminEmail.$invalid">
                                                  Portal Admin Email  is mandatory.
                                                       </p>			 
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->





								</div>
								<div class="form-actions">
									<!-- <button type="submit" class="btn btn-success">
										<i class="fa fa-check"></i>Update
									</button> -->
									<!-- <button type="button" class="btn btn-default">Cancel</button> -->
								</div>
						
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  

  </div>
  
</div>
 </form>

	
	<div class="modal fade" id="companySuccessModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-thumbs-up"></i>Information</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="companyFailModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#companySuccessModel" id="companySuccessClick" style="display:none"/>
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#companyFailModel" id="companyFailClick" style="display:none"/>
  
  
</div>
<!-- /#wrapper -->
<!-- jQuery -->


<script src="./angular/angular.js"></script>
<script src="./angular/corp-home-angularjs.js"></script>

<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>


<!-- end - This is for export functionality only -->

<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    });
  });
    });
    $('#example23').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
  </script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
