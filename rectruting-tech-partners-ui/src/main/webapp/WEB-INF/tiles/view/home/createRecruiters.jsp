<head>
<title>Companies</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="./js/jquery.min.js"></script>
  </head>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>


<div id="wrapper" ng-app="myApp" ng-controller="recuriterCtl">
<form  role="form" name="recuriter" ng-submit="createsubmitForm()">

  <!-- Navigation -->
  
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">recuriter Form</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!--.row-->
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-info">
            <div class="panel-heading"> Enter recuriter Details</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
              <div class="form-body">
                
                <hr>
              
                
                <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Name*</label>
                      <input type="text" id="firstName" class="form-control"  ng-model="recuriter.recruiterName"  required="">
                       </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">First Name *</label>
                      <input type="text" id="firstName" class="form-control"  ng-model="recuriter.firstName"  required="">
                       </div>
                  </div>
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Middle Name *</label>
                      <input type="text" id="middleName" class="form-control"  ng-model="recuriter.middleName"  required="">
                       </div>
                  </div>
                  <!--/span-->
              
                <!--/row-->
              
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Last Name *</label>
                      <input type="text" id="lastName" class="form-control"  ng-model="recuriter.lastName"  required="" >
                       </div>
                  </div>
                  <!--/span-->
                   <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Join Date *</label>
                      
                      <input type="text" id="joinDate" class="form-control"  ng-model="recuriter.joinDate"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
              
              
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Date Of Birth *</label>
                      <input type="text" id="dateOfBirth" class="form-control"  ng-model="recuriter.birthDate"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Personal Email *</label>
                      <input type="email" id="personalEmail" class="form-control"  ng-model="recuriter.personalEmail"  required="">
                       </div>
                  </div>
                  <!--/span-->
               
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Official Email *</label>
                      <input type="email" id="officialEmail" class="form-control"  ng-model="recuriter.officialEmail"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Highest Qualification *</label>
                      <input type="text" id="highestQualification" class="form-control"  ng-model="recuriter.highestQualifincation"  required="">
                       </div>
                  </div>
                  <!--/span-->
              
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Passed Out Date *</label>
                      <input type="text" id="passedOutDate" class="form-control"  ng-model="recuriter.passDate"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Recruiting Comments *</label>
                      <input type="textarea" id="recruitingComments" class="form-control"  ng-model="recuriter.recrutingComments"  required="">
                       </div>
                  </div>
                  <!--/span-->
             
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Salary *</label>
                      <input type="text" id="salary" class="form-control"  ng-model="recuriter.salary"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Commission *</label>
                      <input type="text" id="commission" class="form-control"  ng-model="recuriter.commission"  required="">
                       </div>
                  </div>
                  <!--/span-->
             
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Pervious Experience *</label>
                      <input type="text" id="perviousExperience" class="form-control"  ng-model="recuriter.prevExp"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Previous Company Worked *</label>
                      <input type="text" id="previousCompanyWorked" class="form-control"  ng-model="recuriter.prevCompWorked"  required="">
                       </div>
                  </div>
                  <!--/span-->
              
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Native Place *</label>
                      <input type="text" id="nativePlace" class="form-control"  ng-model="recuriter.nativePlace"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Current Address Line 1 *</label>
                      <input type="text" id="currentAddressLine 1 *" class="form-control"  ng-model="recuriter.addrLine1"  required="">
                       </div>
                  </div>
                  <!--/span-->
              
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Current Address Line 2 *</label>
                      <input type="text" id="currentAddressLine2" class="form-control"  ng-model="recuriter.addrLine2"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Current Address Line 3</label>
                      <input type="text" id="currentAddressLine3" class="form-control"  ng-model="recuriter.addrLine3"  required="">
                       </div>
                  </div>
                  <!--/span-->
              
				<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">State *</label>
                      <input type="text" id="state" class="form-control"  ng-model="recuriter.state"  required="">
                       </div>
                  </div>
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Town/City *</label>
                      <input type="text" id="townCity" class="form-control"  ng-model="recuriter.town"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">District *</label>
                      <input type="text" id="district" class="form-control"  ng-model="recuriter.district"  required="">
                       </div>
                  </div>
                  <!--/span-->
                
						
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Zip Code *</label>
                      <input type="text" id="zipCode" class="form-control"  ng-model="recuriter.zipcode"  required="" pattern="^[0-9]{5}(?:-[0-9]{4})?$" placeholder="Eg:- 12345-6789">
                       </div>
                  </div>
                  <!--/span-->
               
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address Line 1 *</label>
                      <input type="text" id="permanentAddressLine1" class="form-control"  ng-model="recuriter.permAddrLine1"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address Line 2 *</label>
                      <input type="text" id="permanentAddressLine2" class="form-control"  ng-model="recuriter.permAddrLine2"  required="">
                       </div>
                  </div>
                  <!--/span-->
               
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address Line 3</label>
                      <input type="text" id="permanentAddressLine3" class="form-control"  ng-model="recuriter.permAddrLine3"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address Town/City*</label>
                      <input type="text" id="permanentAddressTownCity" class="form-control"  ng-model="recuriter.permtTown"  required="">
                       </div>
                  </div>
                  
				
			
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address District *</label>
                      <input type="text" id="permanentAddressDistrict" class="form-control"  ng-model="recuriter.permDistrict"  required="">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address State *</label>
                      <input type="text" id="permanentAddressState" class="form-control"  ng-model="recuriter.permState"  required="">
                       </div>
                  </div>
                  <!--/span-->
               
				
				
				
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Permanent Address Zip Code *</label>
                      <input type="text" id="permanentAddressZipCode" class="form-control"  ng-model="recuriter.permZipcode"  required="" pattern="^[0-9]{5}(?:-[0-9]{4})?$" placeholder="Eg:- 12345-6789">
                       </div>
                  </div>
                 
                  <!--/span-->
             
                
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Save</button>
                <button type="button" class="btn btn-default">Cancel</button>
              </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
    </form>
    
    
    	<div class="modal fade" id="successModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-thumbs-up"></i>Information</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="failModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#successModel" id="successClick" style="display:none"/>
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#failModel" id="failClick" style="display:none"/>
  
    
    
  </div>
  
  
  
  <script src="./angular/angular.js"></script>
<script src="./angular/recruiter-angularjs.js"></script>
  
  
<!-- /#wrapper -->
<!-- jQuery -->
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Flot Charts JavaScript -->
<script src="../plugins/bower_components/flot/jquery.flot.js"></script>
<script src="../plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<!-- google maps api -->
<script src="../plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="../plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline charts -->
<script src="../plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART JS -->
<script src="../plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="../plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<script src="js/dashboard2.js"></script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$( function() {
    $( "#dateOfBirth" ).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1960:2030"
    });
    
    
    $( "#joinDate" ).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1960:2030"
    });
    
    
    $( "#passedOutDate" ).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1960:2030"
    });
    
    
    
  } );
</script>
