<!DOCTYPE html>
<html lang="en">
<head>
<title>Admins</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="./js/jquery.min.js"></script>
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/custom.css" id="theme"  rel="stylesheet">

<link href="css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="fix-sidebar">
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper"  ng-app="myApp" ng-controller="adminSearchCtl">
  
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
     <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Roles Form</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
    <form  role="form" name="createRole" ng-submit="createRoleForm()">
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-info">
            <div class="panel-heading"></div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
              <div class="form-body">
                
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Select Company *</label>
               <select id="selectCompany"  class="form-control"  required="">
                       <option value="">Select</option>
			         <option ng-repeat=" " ng-value=""></option>
	           </select>
                  </div>
                  </div>
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Select User *</label>
               <select id="selectUser"  class="form-control"  required="">
                       <option value="">Select</option>
			         <option ng-repeat=" " ng-value=""></option>
	           </select>
                  </div>
                  </div>
                  <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Select Role</label>
               <select id="selectRole"  class="form-control"  required="">
                       <option value="">Select</option>
			         <option ng-repeat=" " ng-value=""></option>
	           </select>
                  </div>
                  </div>
                </div>
                
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
               <a href="/roles" class="btn btn-default">Cancel</a>
              </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
      <!--./row-->
      </form> 
     
      <div class="row">
        
        <div class="col-sm-12">
          <div class="white-box">
           <!--  <h3 class="box-title m-b-0">Registered Companies</h3>
            <p class="text-muted m-b-30">Export data to Copy, CSV, Excel, PDF & Print</p> -->
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Company</th>
                            <th>User</th>
                            <th>Role</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                    <tr ng-repeat="role in roles">
                    <td class="span2" ng-click="update(corp)" >{{admin.userName}}</td>
                    <td class="span2" >{{admin.firstName}}</td>
                    <td class="span2" >{{admin.lastName}}</td>
                    <td class="span2" ><input type="button" ng-click="update(admin)" value="Edit"></td>
		            <td class="span2" ><input type="button" ng-click="deleteAdmin(admin)" value="Remove"></td>
                   </tr>
                       
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- .right-sidebar -->
      <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i><> </div>
          <div class="r-panel-body">
            <ul>
              <li><b>Layout Options</b></li>
              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-warning">
                  <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                  <label for="checkbox2" > Fix Sidebar </label>
                </div>
              </li>
              <li>
                <div class="checkbox checkbox-success">
                  <input id="checkbox4" type="checkbox" class="open-close">
                  <label for="checkbox4" > Toggle Sidebar </label>
                </div>
              </li>
            </ul>
            <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 chatonline">
              <li><b>Chat option</b></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small><></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small><></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /.right-sidebar -->
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2016 &copy; Elite Admin brought to you by themedesigner.in </footer>
  </div>
  <!-- /#page-wrapper -->
  
  

   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#updatemodal" id="updatemodalId" style="display:none"/>
   

<!-- Modal -->
<form  role="form" name="adminUpdateForm" ng-submit="updateForm()">
<div id="updatemodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">AdminInfo</h4>
      </div>
      <div class="modal-body">
     
		<div class="form-body">
                
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Company Name *</label>
               <select id="corpSelect"  class="form-control" ng-model="admin.corpId" required="">
                       <option value="">Select Your Company</option>
			         <option ng-repeat="corp in corps" ng-value="corp.id">{{corp.companyName}}</option>
	           </select>
                  </div>
                  </div>
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">User Name / ID *</label>
                      <input type="text" id="userNameId" class="form-control" required="" ng-model="admin.userName">
                       </div>
                  </div>
                  <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">First Name *</label>
                      <input type="text" id="firstName" class="form-control" required="" ng-model="admin.firstName">
                       </div>
                  </div>
                  <!--/span-->
                   <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Last Name *</label>
                      <input type="text" id="lastName" class="form-control" required="" ng-model="admin.lastName">
                       </div>
                  </div>
                 
                  <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Email *</label>
                      <input type="email" id="email" class="form-control" required="" ng-model="admin.email">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Password *</label>
                      <input type="text" id="password" class="form-control" required="" ng-model="admin.password">
                       </div>
                  </div>
                  <!--/span-->
                </div>
                <!--/row-->
                
              </div>
						
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  

  </div>
  
</div>
 </form>

	
	
    	<div class="modal fade" id="successModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-thumbs-up"></i>Information</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="failModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#successModel" id="successClick" style="display:none"/>
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#failModel" id="failClick" style="display:none"/>
  
    
  
</div>
<!-- /#wrapper -->
<!-- jQuery -->


<script src="./angular/angular.js"></script>
<script src="./angular/admin-angularjs.js"></script>

<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>


<!-- end - This is for export functionality only -->

<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    });
  });
    });
    $('#example23').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
  </script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
