<!-- Left navbar-header end -->
<!-- Page Content -->
<div id="page-wrapper" ng-app="myApp" ng-controller="RootCtrl">
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Company Form</h4>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!--.row-->
		<div class="row">
			<div class="col-md-12">

				<div class="panel panel-info">
					<div class="panel-heading">Enter Company Details</div>
					<div class="panel-wrapper collapse in" aria-expanded="true">
						<div class="panel-body">
							<form action="#"  role="form" name="companyCreateForm">
								<div class="form-body">
									<h3 class="box-title">Company Info</h3>
									<hr>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Company Name *</label> 
												<input type="text" class="form-control"  ng-model="companyName" required name="companyName">
												<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyName.$invalid">
                                                 Company Name mandatory.
                                           </p>
											</div>
											
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Company Address Line 1
													*</label>
													 <input type="text" id="companyAddressLine1" ng-model="companyAddressLine1" class="form-control" name="companyAddressLine1" required>
											<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyAddressLine1.$invalid">
                                                CompanyAddressLine1 is mandatory.
                                           </p>		 
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Company Address Line 2
													*</label> <input type="text" id="companyAddressLine2" ng-model="companyAddressLine2" class="form-control" name="companyAddressLine2" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.companyAddressLine2.$invalid">
                                               CompanyAddressLine2 is mandatory.
                                                </p>		
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">City *</label> <input
													type="text" id="city" class="form-control"  ng-model="city" name="city" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.city.$invalid">
                                               City is mandatory.
                                                </p>	
											</div>
										</div>

										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">State *</label> <input
													type="text" id="state" class="form-control"  ng-model="state" name="state" required>
														<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.state.$invalid">
                                                         State is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Zip Code *</label> <input
													type="text" id="state" class="form-control"  ng-model="zip" name="zip" pattern="^[0-9]{5}(?:-[0-9]{4})?$" placeholder="Eg:- 12345-6789" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.zip.$invalid">
                                                         Zip is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->



									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Alert Email Address *</label> <input
													type="email" id="alertEmailAddress" class="form-control"
													 ng-model="alertEmailAddress" name="alertEmailAddress" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.alertEmailAddress.$invalid">
                                                         Alert Email Address is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Interview Integration
													Email *</label> <input type="email" id="interviewIntegrationEmail"
													class="form-control"  ng-model="interviewIntegrationEmail" name="interviewIntegrationEmail" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.interviewIntegrationEmail.$invalid">
                                                        Interview Integration is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">FTIN Number *</label> <input
													type="text" id="ftinNumber" class="form-control" 
													 ng-model="ftinNumber" name="ftinNumber"   pattern="^\(?\d{3}\)?[- ]?\d{2}[- ]?\d{4}$" placeholder="Format : XXX-XX-XXXX">
													 
													   <span ng-show="companyCreateForm.ftinNumber.$error.pattern" style="color:red">Please enter correct email address.</span>
													  <!-- <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ftinNumber.$invalid" style="color:red">
                                                        FTIN Number is mandatory.
                                                       </p>	 -->
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority First
													Name *</label> <input type="text" id="signingAuthorityFirstName"
													class="form-control"  ng-model="signingAuthorityFirstName" name="signingAuthorityFirstName" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityFirstName.$invalid">
                                                        Signing Authority First is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority
													Middle Name *</label> <input type="text"
													id="signingAuthorityMiddleName" class="form-control"
													 ng-model="signingAuthorityMiddleName" name="signingAuthorityMiddleName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityMiddleName.$invalid">
                                                        Signing Authority Middle Name is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority Last
													Name *</label> <input type="text" id="signingAuthorityLastName"
													class="form-control"  ng-model="signingAuthorityLastName" name="signingAuthorityLastName" required>
													
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityLastName.$invalid">
                                                        Signing Authority Last Name is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority Phone
													Number *</label> <input type="text"
													id="signingAuthorityPhoneNumber" class="form-control" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$" placeholder="Eg : +1 (234) 56 89 901"
													 ng-model="signingAuthorityPhoneNumber" name="signingAuthorityPhoneNumber" required>
													<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityPhoneNumber.$invalid">
                                                        Signing Authority PhoneNumber is mandatory.
                                                       </p>	 
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority
													Designation *</label> <input type="text"
													id="signingAuthorityDesignation" class="form-control"
													 ng-model="signingAuthorityDesignation" name="signingAuthorityDesignation" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityDesignation.$invalid">
                                                        Signing AuthorityDesignation is mandatory.
                                                       </p>	 
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Signing Authority Email
													*</label> <input type="email" id="signingAuthorityEmail"
													class="form-control"  ng-model="signingAuthorityEmail" name="signingAuthorityEmail" required>
													
														<p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.signingAuthorityEmail.$invalid">
                                                        Signing Authority Email is mandatory.
                                                       </p>	 
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Owner First Name *</label> <input
													type="text" id="ownerFirstName" class="form-control"
													 ng-model="ownerFirstName" name="ownerFirstName" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerFirstName.$invalid">
                                                        Owner First Name is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Owner Middle Name *</label> <input
													type="text" id="ownerMiddleName" class="form-control"
													 ng-model="ownerMiddleName" name="ownerMiddleName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerMiddleName.$invalid">
                                                        Owner Middle Name is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Owner Last Name *</label> <input
													type="text" id="ownerLastName" class="form-control"
													 ng-model="ownerLastName" name="ownerLastName" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerLastName.$invalid">
                                                        Owner Last Name is mandatory.
                                                       </p>	
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Owner Phone Number *</label> <input
													type="text" id="ownerPhoneNumber" class="form-control" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$" placeholder="Eg : +1 (234) 56 89 901"
													 ng-model="ownerPhoneNumber" name="ownerPhoneNumber" required>
													  <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerPhoneNumber.$invalid">
                                                        Owner Phone Number is mandatory.
                                                       </p>	
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Owner Alternate Phone
													Number *</label> <input type="text" id="ownerAlternatePhoneNumber" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$" placeholder="Eg : +1 (234) 56 89 901"
													class="form-control"  ng-model="ownerAlternatePhoneNumber" name="ownerAlternatePhoneNumber" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.ownerAlternatePhoneNumber.$invalid">
                                                        Owner Alternate PhoneNumber is mandatory.
                                                       </p>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Invoice Contact First
													Name *</label> <input type="text" id="invoiceContactFirstName"
													class="form-control"  ng-model="invoiceContactFirstName" required name="invoiceContactFirstName">
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactFirstName.$invalid">
                                                       Invoice Contact FirstName is mandatory.
                                                       </p>		
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Invoice Contact Middle
													Name *</label> <input type="text" id="invoiceContactMiddleName"
													class="form-control"  ng-model="invoiceContactMiddleName" name="invoiceContactMiddleName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactMiddleName.$invalid">
                                                      Invoice Contact MiddleName  is mandatory.
                                                       </p>				
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Invoice Contact Last
													Name *</label> <input type="text" id="invoiceContactLastName"
													class="form-control"  ng-model="invoiceContactLastName" required name="invoiceContactLastName">
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactLastName.$invalid">
                                                     Invoice Contact LastName is mandatory.
                                                       </p>					
													
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Invoice Contact Phone
													Number *</label> <input type="text" id="invoiceContactPhoneNumber" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$" placeholder="Eg : +1 (234) 56 89 901"
													class="form-control"  ng-model="invoiceContactPhoneNumber" name="invoiceContactPhoneNumber" required>
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactPhoneNumber.$invalid">
                                                     Invoice Contact PhoneNumber is mandatory.
                                                       </p>		
													
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Invoice Contact Email *</label>
												<input type="email" id="invoiceContactEmail"
													class="form-control"  ng-model="invoiceContactEmail" required name="invoiceContactEmail">
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.invoiceContactEmail.$invalid">
                                                     Invoice Contact Email is mandatory.
                                                       </p>				
													
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Portal Admin First Name
													*</label> <input type="text" id="portalAdminFirstName"
													class="form-control"  ng-model="portalAdminFirstName" name="portalAdminFirstName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminFirstName.$invalid">
                                                     Portal Admin First Name is mandatory.
                                                       </p>				
													
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->

									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Portal Admin Middle
													Name *</label> <input type="text" id="portalAdminMiddleName"
													class="form-control"  ng-model="portalAdminMiddleName" name="portalAdminMiddleName" required>
													
											 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminMiddleName.$invalid">
                                                    Portal Admin Middle is mandatory.
                                                       </p>				
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Portal Admin Last Name
													*</label> <input type="text" id="portalAdminLastName"
													class="form-control"  ng-model="portalAdminLastName" name="portalAdminLastName" required>
													
								 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminLastName.$invalid">
                                                   Portal Admin Last Name is mandatory.
                                                       </p>							
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->


									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Portal Admin Phone
													Number *</label> <input type="text" id="portalAdminPhoneNumber" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$"
													class="form-control"  placeholder="Eg : +1 (234) 56 89 901" ng-model="portalAdminPhoneNumber" name="portalAdminPhoneNumber" required>
													 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminPhoneNumber.$invalid">
                                                  Portal Admin PhoneNumber is mandatory.
                                                       </p>		
											</div>
										</div>

										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Portal Admin Email *</label> <input
													type="email" id="portalAdminEmail" class="form-control"
													 ng-model="portalAdminEmail" name="portalAdminEmail" required>
										 <p class="validation-error" ng-show="companyCreateFormSubmitAttempt && companyCreateForm.portalAdminEmail.$invalid">
                                                  Portal Admin Email  is mandatory.
                                                       </p>			 
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->





								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-success" ng-click="saveCorp(companyCreateForm)">>
										<i class="fa fa-check"></i> Save
									</button>
									<a href="/createCompany" class="btn btn-default">Cancel</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--./row-->


		<!--./row-->
		<!-- .right-sidebar -->
		<div class="right-sidebar">
			<div class="slimscrollright">
				<div class="rpanel-title">
					Service Panel <span><i class="ti-close right-side-toggle"></i></span>
				</div>
				<div class="r-panel-body">
					<ul>
						<li><b>Layout Options</b></li>
						<li>
							<div class="checkbox checkbox-info">
								<input id="checkbox1" type="checkbox" class="fxhdr"> <label
									for="checkbox1"> Fix Header </label>
							</div>
						</li>
						<li>
							<div class="checkbox checkbox-warning">
								<input id="checkbox2" type="checkbox" checked="" class="fxsdr">
								<label for="checkbox2"> Fix Sidebar </label>
							</div>
						</li>
						<li>
							<div class="checkbox checkbox-success">
								<input id="checkbox4" type="checkbox" class="open-close">
								<label for="checkbox4"> Toggle Sidebar </label>
							</div>
						</li>
					</ul>
					<ul id="themecolors" class="m-t-20">
						<li><b>With Light sidebar</b></li>
						<li><a href="javascript:void(0)" theme="default"
							class="default-theme">1</a></li>
						<li><a href="javascript:void(0)" theme="green"
							class="green-theme">2</a></li>
						<li><a href="javascript:void(0)" theme="gray"
							class="yellow-theme">3</a></li>
						<li><a href="javascript:void(0)" theme="blue"
							class="blue-theme working">4</a></li>
						<li><a href="javascript:void(0)" theme="purple"
							class="purple-theme">5</a></li>
						<li><a href="javascript:void(0)" theme="megna"
							class="megna-theme">6</a></li>
						<li><b>With Dark sidebar</b></li>
						<br />
						<li><a href="javascript:void(0)" theme="default-dark"
							class="default-dark-theme">7</a></li>
						<li><a href="javascript:void(0)" theme="green-dark"
							class="green-dark-theme">8</a></li>
						<li><a href="javascript:void(0)" theme="gray-dark"
							class="yellow-dark-theme">9</a></li>

						<li><a href="javascript:void(0)" theme="blue-dark"
							class="blue-dark-theme">10</a></li>
						<li><a href="javascript:void(0)" theme="purple-dark"
							class="purple-dark-theme">11</a></li>
						<li><a href="javascript:void(0)" theme="megna-dark"
							class="megna-dark-theme">12</a></li>
					</ul>
					<ul class="m-t-20 chatonline">
						<li><b>Chat option</b></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/varun.jpg" alt="user-img"
								class="img-circle"> <span>Varun Dhavan <small
									class="text-success">online</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/genu.jpg" alt="user-img"
								class="img-circle"> <span>Genelia Deshmukh <small
									class="text-warning">Away</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/ritesh.jpg" alt="user-img"
								class="img-circle"> <span>Ritesh Deshmukh <small
									class="text-danger">Busy</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/arijit.jpg" alt="user-img"
								class="img-circle"> <span>Arijit Sinh <small
									class="text-muted">Offline</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/govinda.jpg" alt="user-img"
								class="img-circle"> <span>Govinda Star <small
									class="text-success">online</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/hritik.jpg" alt="user-img"
								class="img-circle"> <span>John Abraham<small
									class="text-success">online</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/john.jpg" alt="user-img"
								class="img-circle"> <span>Hritik Roshan<small
									class="text-success">online</small></span></a></li>
						<li><a href="javascript:void(0)"><img
								src="../plugins/images/users/pawandeep.jpg" alt="user-img"
								class="img-circle"> <span>Pwandeep rajan <small
									class="text-success">online</small></span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /.right-sidebar -->
	</div>
	<!-- /.container-fluid -->
	
	
	
	<div class="modal fade" id="companySuccessModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-thumbs-up"></i>Information</h4>
        </div>
        <div class="modal-body">
          <strong>Corp created successfully.</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="companyFailModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <strong>Unable to create corp.....</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#companySuccessModel" id="companySuccessClick" style="display:none"/>
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#companyFailModel" id="companyFailClick" style="display:none"/>

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="./bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script
	src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="./js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="./js/waves.js"></script>
<!-- Flot Charts JavaScript -->
<script src="../plugins/bower_components/flot/jquery.flot.js"></script>
<script
	src="../plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<!-- google maps api -->
<script
	src="../plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script
	src="../plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline charts -->
<script
	src="../plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART JS -->
<script
	src="../plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script
	src="../plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./js/custom.min.js"></script>
<script src="./js/dashboard2.js"></script>


<script src="./angular/angular.js"></script>
<script src="./angular/corp-home-angularjs.js"></script>



<!--Style Switcher -->
<script
	src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
