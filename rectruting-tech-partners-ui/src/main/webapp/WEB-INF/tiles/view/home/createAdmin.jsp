<head>
<title>Companies</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
 <script src="./js/jquery.min.js"></script>
  </head>
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper" ng-app="myApp" ng-controller="adminCtl">
<form  role="form" name="admin" ng-submit="createsubmitForm()">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Admin Form</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!--.row-->
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-info">
            <div class="panel-heading"> Enter Admin Details</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
              <div class="form-body">
                
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Company Name *</label>
               <select id="corpSelect"  class="form-control" ng-model="admin.corpId" required="">
                       <option value="">Select Your Company</option>
			         <option ng-repeat="corp in corps" ng-value="corp.id">{{corp.companyName}}</option>
	           </select>
                  </div>
                  </div>
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">User Name / ID *</label>
                      <input type="text" id="userNameId" class="form-control" required="" ng-model="admin.userName">
                       </div>
                  </div>
                  <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">First Name *</label>
                      <input type="text" id="firstName" class="form-control" required="" ng-model="admin.firstName">
                       </div>
                  </div>
                  <!--/span-->
                   <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Last Name *</label>
                      <input type="text" id="lastName" class="form-control" required="" ng-model="admin.lastName">
                       </div>
                  </div>
                 
                  <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
						<div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Email *</label>
                      <input type="text" id="email" class="form-control" required="" ng-model="admin.email">
                       </div>
                  </div>
                 
                  <!--/span-->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Password *</label>
                      <input type="text" id="password" class="form-control" required="" ng-model="admin.password">
                       </div>
                  </div>
                  <!--/span-->
                </div>
                <!--/row-->
                
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                <button type="button" class="btn btn-default">Cancel</button>
              </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
      <!--./row-->
   </div>
  </div>
  </form>
  
  
    	<div class="modal fade" id="successModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-thumbs-up"></i>Information</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="failModel" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header modal-header-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <strong>{{message}}</strong>
		 </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
   <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#successModel" id="successClick" style="display:none"/>
  <input type="button" value="Add" class="btn btn-info btn-xs" data-toggle="modal" data-target="#failModel" id="failClick" style="display:none"/>
  </div>
<%@include file="/WEB-INF/tiles/view/home/loadScript.jsp"%>
 <script src="./angular/angular.js"></script>
<script src="./angular/admin-angularjs.js"></script>
