var app = angular.module('corpSearch', []);

app.controller('corpSearchCtl', function($scope, $http) {
	   // alert('this corp search');
	    
	    $http.get("http://localhost:9070/corp/findAllCorps").success(
	    function (response) {
		    $scope.corps=null;
	        $scope.corps = response;
		}).error(function() {
			    $scope.loading = false;
			    alert('Internal server error. Please try again later');
		     });    
  	
		$scope.update = function(corp) {
			//alert('clicked'+corp.companyName);
			$scope.corp=corp;
		angular.element('#updatemodalId').trigger('click');		
		}
});