var app = angular.module('myApp', []);

app.controller('recuriterCtl', function($scope, $http) {
	
	$scope.rootUrl="";
	 $http.get("./getUser").success(function (response) {
		   //alert(response);
		   var array = response.split('&&');
		   $scope.user=array[0];
	       $scope.rootUrl=array[1];
		}).error(function() {
			$scope.loading = false;
	}); 
	
	$scope.createsubmitForm = function() {
		
		$scope.recuriter.userId=15;
		$scope.recuriter.corpId=10;
        var json = JSON.stringify($scope.recuriter);
		console.log(json);
		var res = $http.post($scope.rootUrl+'recruiter/create', json);
		res.success(function(data, status, headers, config) {
			$scope.message="Recuritercreated successfully...";
			$scope.recuriter=null;
			angular.element('#successClick').trigger('click');		
		});
		res.error(function(data, status, headers, config) {
			$scope.message="Unable to create Recuriterplease try later...";
			angular.element('#failClick').trigger('click');
		});		
		
	};
  	
});


app.controller('recruitersSearchCtl', function($scope, $http) {
	
	
	
	$scope.rootUrl="";
	 $http.get("./getUser").success(function (response) {
		   //alert(response);
		   var array = response.split('&&');
		   $scope.user=array[0];
	       $scope.rootUrl=array[1];
	       $http.get($scope.rootUrl+"recruiter/findAllRecruiters").success(
	    		    function (response) {
	    			    $scope.recuriters=null;
	    		        $scope.recuriters = response;
	    			}).error(function() {
	    				    $scope.loading = false;
	    				    alert('Internal server error. Please try again later');
	    			     });    
		}).error(function() {
			$scope.loading = false;
	}); 
	
	
	
	

	
	$scope.update = function(recuriter) {
		//alert('clicked'+corp);
		console.log(recuriter);
		$scope.recuriter=recuriter;
	angular.element('#updatemodalId').trigger('click');		
	}
	
	$scope.deleteRecuriter = function(recuriter) {
	
		var dataObj = {
				id : recuriter.id
		}
	  var json = JSON.stringify(dataObj);
		console.log(json);
		var res = $http.post($scope.rootUrl+'recruiter/deleteRecruiter', json);
		res.success(function(data, status, headers, config) {
			$scope.message="Company deleted successfully...";
			angular.element('#successClick').trigger('click');	
			
			 $http.get($scope.rootUrl+"recruiter/findAllRecruiters").success(
					    function (response) {
						    $scope.recuriters=null;
					        $scope.recuriters = response;
						}).error(function() {
							    $scope.loading = false;
							     //alert('Internal server error. Please try again later');
						     });    
		});
		res.error(function(data, status, headers, config) {
			$scope.message="Unable to delete company please try later...";
			angular.element('#failClick').trigger('click');
		});		
	}
	
	
	
	
	$scope.updateForm = function() {
		//alert('hiii'+$scope.corp);
		$scope.recuriter.userId=15;
		$scope.recuriter.corpId=10;
        var json = JSON.stringify($scope.recuriter);
		console.log(json);
		
		var res = $http.post($scope.rootUrl+'recruiter/updateRecruiter', json);
		res.success(function(data, status, headers, config) {
			$scope.message="Recuriter update successfully...";
			angular.element('#successClick').trigger('click');		
		});
		res.error(function(data, status, headers, config) {
			$scope.message="Unable to update Recuriter please try later...";
			angular.element('#failClick').trigger('click');
		});		
	
		
	    };
});





