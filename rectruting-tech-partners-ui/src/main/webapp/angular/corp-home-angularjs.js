var app = angular.module('myApp', []);

app.controller('RootCtrl', function($scope, $http) {
	$scope.saveCorp = function(ngForm) {
		$scope.rootUrl="";
		 $http.get("./getUser").success(function (response) {
			   //alert(response);
			   var array = response.split('&&');
			   $scope.user=array[0];
		       $scope.rootUrl=array[1];
			}).error(function() {
				$scope.loading = false;
		});    
		
		
		//alert($scope.companyName);
		
		
		if(ngForm.$invalid) {
		//	alert('fail');
				 $scope.companyCreateFormSubmitAttempt = true;
                return;
            }else{
            	//alert('succ');
				$scope.companyCreateFormSubmitAttempt = false;
			}
		
		var dataObj = {
				companyName : $scope.companyName,
				addressLine1 : $scope.companyAddressLine1,
				addressLine2 : $scope.companyAddressLine2,
				city : $scope.city,
				state : $scope.state,
				zip : $scope.zip,
				eodAlertEmail : $scope.alertEmailAddress,
				interviewIntegrationEmail : $scope.interviewIntegrationEmail,
				ftinNumber : $scope.ftinNumber,
				signingAuthorityFirstName : $scope.signingAuthorityFirstName,
				signingAuthorityMiddleName : $scope.signingAuthorityMiddleName,
				signingAuthorityLastName : $scope.signingAuthorityLastName,
				signingAuthorityPhone : $scope.signingAuthorityPhoneNumber,
				signingAuthorityDesignation : $scope.signingAuthorityDesignation,
				signingAuthorityEmail : $scope.signingAuthorityEmail,
				ownerFirstName : $scope.ownerFirstName,
				ownerMiddleName : $scope.ownerMiddleName,
				ownerLastName : $scope.ownerLastName,
				ownerPhone : $scope.ownerPhoneNumber,
				ownerAlternatePhone : $scope.ownerAlternatePhoneNumber,
				invoiceContactFirstName : $scope.invoiceContactFirstName,
				invoiceContactMiddleName : $scope.invoiceContactMiddleName,
				invoiceContactLastName : $scope.invoiceContactLastName,
				invoiceContactPhone : $scope.invoiceContactPhoneNumber,
				invoiceContactEmail : $scope.invoiceContactEmail,
				portalAdminFirstName : $scope.portalAdminFirstName,
				portalAdminMiddleName : $scope.portalAdminMiddleName,
				portalAdminLastName : $scope.portalAdminLastName,
				portalAdminPhone : $scope.portalAdminPhoneNumber,
				portalAdminEmail : $scope.portalAdminEmail,
				userId : $scope.portalAdminEmail
		};	
		
		var json = JSON.stringify(dataObj);
		
		console.log(json);
		
		//alert(json);
		
		var res = $http.post($scope.rootUrl+'corp/create', json);
		res.success(function(data, status, headers, config) {
			
			$scope.companyName=null;
			$scope.companyAddressLine1=null;
			$scope.companyAddressLine2=null;
			$scope.city=null;
			$scope.state=null;
			$scope.zip=null;
			
			$scope.alertEmailAddress=null;
			$scope.interviewIntegrationEmail=null;
			$scope.ftinNumber=null;
			$scope.signingAuthorityFirstName=null;
			$scope.signingAuthorityMiddleName=null;
			$scope.signingAuthorityLastName=null;
			$scope.signingAuthorityPhoneNumber=null;
			$scope.signingAuthorityDesignation=null;
			$scope.signingAuthorityEmail=null;
			$scope.ownerFirstName=null;
			$scope.ownerMiddleName=null;
			$scope.ownerLastName=null;
			$scope.ownerPhoneNumber=null;
			$scope.ownerAlternatePhoneNumber=null;
			$scope.invoiceContactFirstName=null;
			$scope.invoiceContactMiddleName=null;
			$scope.invoiceContactLastName=null;
			$scope.invoiceContactPhoneNumber=null;
			$scope.invoiceContactEmail=null;
			$scope.portalAdminFirstName=null;
			$scope.portalAdminMiddleName=null;
			$scope.portalAdminLastName=null;
			$scope.portalAdminPhoneNumber=null;
			$scope.portalAdminEmail=null;
			$scope.portalAdminEmail=null;
			angular.element('#companySuccessClick').trigger('click');		
		});
		res.error(function(data, status, headers, config) {
			angular.element('#companyFailClick').trigger('click');
			//alert( "failure message: " + JSON.stringify({data: data}));
		});		
		
	    
	 }
  	
});


app.controller('corpSearchCtl', function($scope, $http) {
	$scope.rootUrl='';
	 $http.get("./getUser").success(function (response) {
		   //alert(response);
		   var array = response.split('&&');
		   $scope.user=array[0];
	       $scope.rootUrl=array[1];
	      // alert( $scope.rootUrl+":::::::::::::hello");
	      
	       $http.get($scope.rootUrl+"corp/findAllCorps").success(
	    		    function (response) {
	    			    $scope.corps=null;
	    		        $scope.corps = response;
	    			}).error(function() {
	    				    $scope.loading = false;
	    				    alert('Internal server error. Please try again later');
	    	}); 
	        
		}).error(function() {
			    $scope.loading = false;
	});    
	
	
      
	
	$scope.update = function(corp) {
		//alert('clicked'+corp);
		console.log(corp);
		$scope.corp=corp;
	angular.element('#updatemodalId').trigger('click');		
	}
	
	$scope.deleteCorp = function(corp) {
	
		var dataObj = {
				id : corp.id
		}
	  var json = JSON.stringify(dataObj);
		console.log(json);
		var res = $http.post($scope.rootUrl+'corp/deleteCorp', json);
		res.success(function(data, status, headers, config) {
			$scope.message="Company deleted successfully...";
			angular.element('#companySuccessClick').trigger('click');	
			
			 $http.get($scope.rootUrl+"corp/findAllCorps").success(
					    function (response) {
						    $scope.corps=null;
					        $scope.corps = response;
						}).error(function() {
							    $scope.loading = false;
							   // alert('Internal server error. Please try again later');
						     });    
		});
		res.error(function(data, status, headers, config) {
			$scope.message="Unable to delete company please try later...";
			angular.element('#companyFailClick').trigger('click');
		});		
	}
	
	
	
	
	$scope.submitForm = function() {
		//alert('hiii'+$scope.corp);
		$scope.corp.userId=15;
        var json = JSON.stringify($scope.corp);
		console.log(json);
		
		var res = $http.post( $scope.rootUrl+'corp/updateCorp', json);
		res.success(function(data, status, headers, config) {
			$scope.message="Company update successfully...";
			angular.element('#companySuccessClick').trigger('click');		
		});
		res.error(function(data, status, headers, config) {
			$scope.message="Unable to update company please try later...";
			angular.element('#companyFailClick').trigger('click');
		});		
	
		
	    };
});





