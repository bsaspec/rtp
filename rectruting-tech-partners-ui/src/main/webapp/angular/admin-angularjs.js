var app = angular.module('myApp', []);

app.controller('adminCtl', function($scope, $http) {
	
	
	$scope.rootUrl="";
	 $http.get("./getUser").success(function (response) {
		   //alert(response);
		   var array = response.split('&&');
		   $scope.user=array[0];
	       $scope.rootUrl=array[1];
	       
	       $http.get($scope.rootUrl+"corp/findAllCorps").success(
	   			function(response) {
	   				$scope.corps = null;
	   				$scope.corps = response;
	   			}).error(function() {
	   		$scope.loading = false;
	   		alert('Internal server error. Please try again later');
	   	});
		}).error(function() {
			$scope.loading = false;
	}); 
	
	

	$scope.createsubmitForm = function() {
		$scope.admin.userId = 15;
		//alert($scope.admin.corpId);
	
		var json = JSON.stringify($scope.admin);
		console.log(json);
		var res = $http.post($scope.rootUrl+'admin/create', json);
		res.success(function(data, status, headers, config) {
			$scope.message = "Admin created successfully...";
			$scope.recuriter = null;
			angular.element('#successClick').trigger('click');
		});
		res.error(function(data, status, headers, config) {
			$scope.message = "Unable to create Admin please try later...";
			angular.element('#failClick').trigger('click');
		});

	};

});

app.controller('adminSearchCtl', function($scope, $http) {
	
	
	$scope.rootUrl="";
	 $http.get("./getUser").success(function (response) {
		   //alert(response);
		   var array = response.split('&&');
		   $scope.user=array[0];
	       $scope.rootUrl=array[1];
	       
	       
	   	$http.get($scope.rootUrl+"admin/findAllUsers").success(function(response) {
					$scope.admins = null;
					$scope.admins = response;
				}).error(function() {
			$scope.loading = false;
			alert('Internal server error. Please try again later');
		});
		}).error(function() {
			$scope.loading = false;
	}); 
	
	


	$scope.update = function(admin) {
		// alert('clicked'+corp);
		
		$http.get($scope.rootUrl+"corp/findAllCorps").success(
				function(response) {
					$scope.corps = null;
					$scope.corps = response;
				}).error(function() {
			$scope.loading = false;
			alert('Internal server error. Please try again later');
		});
		
		console.log(admin);
		$scope.admin = admin;
		angular.element('#updatemodalId').trigger('click');
	}

	$scope.deleteAdmin = function(admin) {

		var dataObj = {
			id : admin.id
		}
		var json = JSON.stringify(dataObj);
		console.log(json);
		var res = $http.post($scope.rootUrl+'admin/deleteUser',json);
		res.success(function(data, status, headers, config) {
			$scope.message = "Admin deleted successfully...";
			angular.element('#successClick').trigger('click');
			$http.get($scope.rootUrl+"admin/findAllUsers").success(
					function(response) {
						$scope.admins = null;
						$scope.admins = response;
					}).error(function() {
				$scope.loading = false;
				alert('Internal server error. Please try again later');
			});
		});
		res.error(function(data, status, headers, config) {
			$scope.message = "Admin to delete company please try later...";
			angular.element('#failClick').trigger('click');
			
		});
	}

	$scope.updateForm = function() {
		// alert('hiii'+$scope.corp);
		$scope.admin.userId = 15;
		
		var json = JSON.stringify($scope.admin);
		console.log(json);

		var res = $http.post($scope.rootUrl+'admin/updateUser',json);
		res.success(function(data, status, headers, config) {
			$scope.message = "Admin update successfully...";
			angular.element('#successClick').trigger('click');
		});
		res.error(function(data, status, headers, config) {
			$scope.message = "Unable to update Admin please try later...";
			angular.element('#failClick').trigger('click');
		});

	};
});
